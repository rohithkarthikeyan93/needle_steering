
#include "stdafx.h"
#include <sdkddkver.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <phidget21.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <tchar.h>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

// update : 08/18/2017 - code integration completed
// Requires testing
// Add comments - ASAP!CPhidgetAdvancedServo_setPosition(servo, 0, 0);

using namespace std;

//==================================
//Stepper Variables Begin

int i1 = 1, i2 = 1;
double const newConv = 640;
double insDepth;
int boolStepper = 1;
double const distStepper = 1;
__int64 stepperCurrentPosition;
double joyStartStepper, joyNewStepper;

//Stepper Variables End
//==================================

//==================================
//Servo Variables Start

double const distServo = 10;
double rotServo1, rotServo2;
int boolServo = 1;
int leftFlag = 1, rightFlag = 0;
int bool_inc = 0;
int bool_dec = 0;
double servo2CurrentPosition, servo1CurrentPosition;
double joyNewServo;
double joyStartServo = 0;

// Servo Variables End
//==================================

//==================================
//Phidget Handles Begin

CPhidgetStepperHandle stepper;
CPhidgetInterfaceKitHandle ifkit;
CPhidgetAdvancedServoHandle servo;

// Phidget Handles End
//==================================

//==================================
// User defined functions begin

// Stepper position control function
void Insertion_Control()
{
	if (joyNewStepper > joyStartStepper) // If the current Joystick value is more than the starting value
	{
		insDepth = stepperCurrentPosition + distStepper*i1*newConv;
		joyStartStepper = joyNewStepper;
		if (insDepth == stepperCurrentPosition) // untile position is reached
		{
			i1++;
		}
	}
	else if (joyNewStepper < joyStartStepper) // If current posn < the start val - do something else
	{
		insDepth = stepperCurrentPosition - distStepper*i2*newConv;
		joyStartStepper = joyNewStepper;
		if (insDepth == stepperCurrentPosition)
		{
			i2++;
		}
	}
	else // no change in joystick position
	{
		insDepth = 0;
		joyStartStepper = joyNewStepper;
	}
	cout << "Target_position:" << insDepth << endl;
}

// Servo rotation control function
void Rotation_Control()
{
	if (joyNewServo > joyStartServo)
	{
		bool_inc = 1;
		if (leftFlag == 1)
		{
			rotServo1 = servo1CurrentPosition + distServo;
			joyStartServo = joyNewServo;
		}

		else if (rightFlag == 1)
		{
			rotServo2 = servo2CurrentPosition + distServo;
			joyStartServo = joyNewServo;
		}

	}

	else if (joyNewServo < joyStartServo)
	{
		bool_dec = 1;

		if (leftFlag == 1)
		{
			rotServo1 = servo1CurrentPosition - distServo;
			joyStartServo = joyNewServo;
		}

		else if (rightFlag == 1)
		{
			rotServo2 = servo2CurrentPosition - distServo;
			joyStartServo = joyNewServo;
		}

	}

	else

	{	cout << "No change\n";
		rotServo1 = 0;
		rotServo2 = 0;
		joyStartServo = joyNewServo;
	}



}

// User defined functions end
//==================================

// ============ Call Backs Stepper Begin ================

int CCONV AHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV DHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV EHandler(CPhidgetHandle stepper, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s\n", ErrorCode, Description);
	return 0;
}

int CCONV PCHandler(CPhidgetStepperHandle stepper, void *usrptr, int Index, __int64 Value)
{
	return 0;
}

int DP(CPhidgetStepperHandle phid)
{
	int serialNo, version, numMotors;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetStepper_getMotorCount(phid, &numMotors);


	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n# Motors: %d\n", serialNo, version, numMotors);

	return 0;
}

// ============ Call Backs Stepper End ================

// ============ Call Backs IFK Begin ==================

int CCONV attachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d attached!\n", name, serialNo);

	return 0;
}

int CCONV detachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d detached!\n", name, serialNo);

	return 0;
}

int CCONV errorHandler(CPhidgetHandle IFK, void *userptr, int ErrorCode, const char *unknown)
{
	printf("Error handled. %d - %s", ErrorCode, unknown);
	return 0;
}

int CCONV inputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Input: %d > State: %d\n", Index, State);

	// Stops system run

	if (Index == 0) {
		boolServo = 0;
	}

	// Shifts between active servos
	if (Index == 1 && State == 0)
	{
		leftFlag = 1;
		rightFlag = 0;
	}

	else if (Index == 1 && State == 1)
	{
		leftFlag = 0;
		rightFlag = 1;

	}
	if (Index == 2 && State == 0)
	{
		boolStepper = 0;
	}
	else if (Index == 2 && State == 1)
	{
		boolStepper = 1;
	}

	return 0;
}

int CCONV outputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Output: %d > State: %d\n", Index, State);
	return 0;
}

int CCONV sensorChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int Value)
{
	if (Index == 0) {
		joyNewStepper = Value;
		Insertion_Control();
		joyStartStepper = joyNewStepper;
	}
	else if (Index == 1) {
		joyNewServo = Value;
		Rotation_Control();
	}
	return 0;
}

int Display_Properties(CPhidgetInterfaceKitHandle phid)
{
	int serialNo, version, numInputs, numOutputs, numSensors, triggerVal, ratiometric, i;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetInterfaceKit_getInputCount(phid, &numInputs);
	CPhidgetInterfaceKit_getOutputCount(phid, &numOutputs);
	CPhidgetInterfaceKit_getSensorCount(phid, &numSensors);
	CPhidgetInterfaceKit_getRatiometric(phid, &ratiometric);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("# Digital Inputs: %d\n# Digital Outputs: %d\n", numInputs, numOutputs);
	printf("# Sensors: %d\n", numSensors);
	printf("Ratiometric: %d\n", ratiometric);

	for (i = 0; i < numSensors; i++)
	{
		CPhidgetInterfaceKit_getSensorChangeTrigger(phid, i, &triggerVal);

		printf("Sensor#: %d > Sensitivity Trigger: %d\n", i, triggerVal);
	}

	return 0;
}
// ============ Call Backs IFK End =====================

// ============ Call Backs Servo Begin =================
int CCONV AttachHandler(CPhidgetHandle ADVSERVO, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(ADVSERVO, &name);
	CPhidget_getSerialNumber(ADVSERVO, &serialNo);
	printf("%s %10d attached!\n", name, serialNo);

	return 0;
}

int CCONV DetachHandler(CPhidgetHandle ADVSERVO, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(ADVSERVO, &name);
	CPhidget_getSerialNumber(ADVSERVO, &serialNo);
	printf("%s %10d detached!\n", name, serialNo);

	return 0;
}

int CCONV ErrorHandler(CPhidgetHandle ADVSERVO, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s\n", ErrorCode, Description);
	return 0;
}

int CCONV PositionChangeHandler(CPhidgetAdvancedServoHandle ADVSERVO, void *usrptr, int Index, double Value)
{
	return 0;
}

//Display the properties of the attached phidget to the screen.  We will be displaying the name, serial number and version of the attached device.

int display_properties(CPhidgetAdvancedServoHandle phid)
{
	int serialNo, version, numMotors;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetAdvancedServo_getMotorCount(phid, &numMotors);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n# Motors: %d\n", serialNo, version, numMotors);

	return 0;
}
// ============ Call Backs Servo End =================


int System_Steering_Control()
{
	int result;
	__int64 curr_pos;
	const char *err;
	int stopped;
	int state;

	CPhidgetStepperHandle stepper = 0;
	CPhidgetInterfaceKitHandle ifKit = 0;
	CPhidgetAdvancedServoHandle servo = 0;

	CPhidgetStepper_create(&stepper);
	CPhidgetInterfaceKit_create(&ifKit);
	CPhidgetAdvancedServo_create(&servo);


	CPhidget_set_OnAttach_Handler((CPhidgetHandle)stepper, AHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)stepper, DHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)stepper, EHandler, NULL);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKit, attachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)ifKit, detachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)ifKit, errorHandler, NULL);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle)servo, AttachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)servo, DetachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)servo, ErrorHandler, NULL);
	
	CPhidgetInterfaceKit_set_OnSensorChange_Handler(ifKit, sensorChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnInputChange_Handler(ifKit, inputChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnOutputChange_Handler(ifKit, outputChangeHandler, NULL);

	CPhidgetStepper_set_OnPositionChange_Handler(stepper, PCHandler, NULL);
	CPhidgetAdvancedServo_set_OnPositionChange_Handler(servo, PositionChangeHandler, NULL);

	CPhidget_open((CPhidgetHandle)stepper, 420840); 
	CPhidget_open((CPhidgetHandle)ifKit, 319699);
	CPhidget_open((CPhidgetHandle)servo, 392837);

	printf("Waiting for interface kit to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)ifKit, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	printf("Waiting for Phidget to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)stepper, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)servo, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		for (;;);
		return 0;
	}

	DP(stepper);
	Display_Properties(ifKit);
	display_properties(servo);

	// Initialize Stepper and Servos

	CPhidgetStepper_setAcceleration(stepper, 0, 20000);
	CPhidgetStepper_setVelocityLimit(stepper, 0, 11000);
	CPhidgetStepper_setCurrentLimit(stepper, 0, 1.2); // Current limit set to 1.2 mA

	CPhidgetAdvancedServo_setVelocityLimit(servo, 0, 45);
	CPhidgetAdvancedServo_setAcceleration(servo, 0, 29121);
	CPhidgetAdvancedServo_setEngaged(servo, 0, 1);

	CPhidgetAdvancedServo_setVelocityLimit(servo, 7, 45);
	CPhidgetAdvancedServo_setAcceleration(servo, 7, 29121);
	CPhidgetAdvancedServo_setEngaged(servo, 7, 1);

	if (CPhidgetStepper_getCurrentPosition(stepper, 0, &curr_pos) == EPHIDGET_OK)
		printf("Motor: 0 > Current Position: %lld\n", curr_pos);

	CPhidgetStepper_setCurrentPosition(stepper, 0, 0);
	CPhidgetStepper_setEngaged(stepper, 0, 1);

	boolServo = 1;
	joyStartStepper = 0;
	

	// End Initialize Stepper and Servos
	cout << "boolstepper:" << boolStepper << endl;
	
	do
	{	
		if (boolStepper == 1)
		{	CPhidgetStepper_setTargetPosition(stepper, 0, insDepth);
			CPhidgetStepper_getCurrentPosition(stepper, 0,&stepperCurrentPosition);
		}

		if (leftFlag == 1)
		{
			CPhidgetAdvancedServo_getPosition(servo, 0, &servo1CurrentPosition);
			CPhidgetAdvancedServo_setPosition(servo, 0, rotServo1);
		}
		else {
			CPhidgetAdvancedServo_getPosition(servo, 7, &servo2CurrentPosition);
			CPhidgetAdvancedServo_setPosition(servo, 7, rotServo2);
		}

		bool_inc = 0;
		bool_dec = 0;

	} while (boolServo == 1);

	stopped = PFALSE;

	while (!stopped)
	{
		CPhidgetStepper_getStopped(stepper, 0, &stopped);
	}
	CPhidgetStepper_setEngaged(stepper, 0, 0);

	while (!stopped)
	{
		CPhidgetAdvancedServo_getStopped(servo, 0, &state);
		stopped = state;
	}

	while (!stopped)
	{
		CPhidgetAdvancedServo_getStopped(servo, 7, &state);
		stopped = state;
	}


	printf("Press any key to end\n");
	getchar();

	/*CPhidgetAdvancedServo_setPosition(servo, 0, 0);
	CPhidgetAdvancedServo_setPosition(servo, 7, 0);*/

	stopped = PFALSE;
	while (!stopped)
	{
		CPhidgetAdvancedServo_getStopped(servo, 0, &state);
		stopped = state;
	}


	while (!stopped)
	{
		CPhidgetAdvancedServo_getStopped(servo, 7, &state);
		stopped = state;
	}

	CPhidgetAdvancedServo_setEngaged(servo, 0, 0);
	CPhidgetAdvancedServo_setEngaged(servo, 7, 0);

	printf("Closing...\n");

	CPhidget_close((CPhidgetHandle)stepper);
	CPhidget_delete((CPhidgetHandle)stepper);
	CPhidget_close((CPhidgetHandle)servo);
	CPhidget_delete((CPhidgetHandle)servo);
	CPhidget_close((CPhidgetHandle)ifkit);
	CPhidget_delete((CPhidgetHandle)ifkit);

	return 0;
}

int main(int argc, char* argv[])
{
	System_Steering_Control();
	return 0;
}


