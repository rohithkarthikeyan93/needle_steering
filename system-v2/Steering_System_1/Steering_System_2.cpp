
#include "stdafx.h"
#include <sdkddkver.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <phidget21.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <tchar.h>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>


using namespace std;

clock_t t;
int count_spl;
double start_val, distance_travelled, user_val, old_dutycycle;
int flag = 0;
int count_change = 0;



fstream myfile;

double user_z;
double new_conv = 640;
double sys_z, sys_z2;
double bool_val = 0;
int stop, flex;
double depth, depth2;

double joy_value;
int boolval;

CPhidgetStepperHandle stepper;
CPhidgetInterfaceKitHandle ifkit;
CPhidgetAdvancedServoHandle aservo;


// ============ Call Back Events for Stepper ==================================

int CCONV AHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV DHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV EHandler(CPhidgetHandle stepper, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s\n", ErrorCode, Description);
	return 0;
}

int CCONV PCHandler(CPhidgetStepperHandle stepper, void *usrptr, int Index, __int64 Value)
{


	return 0;
}

int DP(CPhidgetStepperHandle phid)
{
	int serialNo, version, numMotors;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetStepper_getMotorCount(phid, &numMotors);


	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n# Motors: %d\n", serialNo, version, numMotors);

	return 0;
}
// ============================================================================


// ============ Call Back Events for IFKit ==================================

int CCONV attachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d attached!\n", name, serialNo);

	return 0;
}

int CCONV detachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d detached!\n", name, serialNo);

	return 0;
}

int CCONV errorHandler(CPhidgetHandle IFK, void *userptr, int ErrorCode, const char *unknown)
{
	printf("Error handled. %d - %s", ErrorCode, unknown);
	return 0;
}

int CCONV inputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Input: %d > State: %d\n", Index, State);
	boolval = State;
	return 0;
}

int CCONV outputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Output: %d > State: %d\n", Index, State);
	return 0;
}

int CCONV sensorChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int Value)
{
	printf("Sensor: %d > Value: %d\n", Index, Value);
	
	if (Value > 399 && Value < 601)
	{
		joy_value = 0;
	}
	
	else if(Value < 400)

	{
		joy_value = (100/399)*(Value)-100;
	}
	
	else if(Value > 600)
	{
		joy_value = (900/601)*Value+100;
	}
	return 0;
}

int display_properties(CPhidgetInterfaceKitHandle phid)
{
	int serialNo, version, numInputs, numOutputs, numSensors, triggerVal, ratiometric, i;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetInterfaceKit_getInputCount(phid, &numInputs);
	CPhidgetInterfaceKit_getOutputCount(phid, &numOutputs);
	CPhidgetInterfaceKit_getSensorCount(phid, &numSensors);
	CPhidgetInterfaceKit_getRatiometric(phid, &ratiometric);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("# Digital Inputs: %d\n# Digital Outputs: %d\n", numInputs, numOutputs);
	printf("# Sensors: %d\n", numSensors);
	printf("Ratiometric: %d\n", ratiometric);

	for (i = 0; i < numSensors; i++)
	{
		CPhidgetInterfaceKit_getSensorChangeTrigger(phid, i, &triggerVal);

		printf("Sensor#: %d > Sensitivity Trigger: %d\n", i, triggerVal);
	}

	return 0;
}

// ============================================================================
int interfacekit_simple()
{
	int result, numSensors, i;
	const char *err;

	//Declare an InterfaceKit handle
	CPhidgetInterfaceKitHandle ifKit = 0;

	//create the InterfaceKit object
	CPhidgetInterfaceKit_create(&ifKit);

	//Set the handlers to be run when the device is plugged in or opened from software, unplugged or closed from software, or generates an error.
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKit, attachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)ifKit, detachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)ifKit, errorHandler, NULL);

	//Registers a callback that will run if an input changes.
	//Requires the handle for the Phidget, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetInterfaceKit_set_OnInputChange_Handler(ifKit, inputChangeHandler, NULL);

	//Registers a callback that will run if the sensor value changes by more than the OnSensorChange trig-ger.
	//Requires the handle for the IntefaceKit, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetInterfaceKit_set_OnSensorChange_Handler(ifKit, sensorChangeHandler, NULL);

	//Registers a callback that will run if an output changes.
	//Requires the handle for the Phidget, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetInterfaceKit_set_OnOutputChange_Handler(ifKit, outputChangeHandler, NULL);

	//open the interfacekit for device connections
	CPhidget_open((CPhidgetHandle)ifKit, 323855);

	//get the program to wait for an interface kit device to be attached
	printf("Waiting for interface kit to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)ifKit, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	//Display the properties of the attached interface kit device
	display_properties(ifKit);

	//read interface kit event data
	printf("Reading.....\n");

	//keep displaying interface kit data until user input is read
	printf("Press any key to go to next step\n");
	getchar();

	printf("Modifying sensor sensitivity triggers....\n");

	//get the number of sensors available
	CPhidgetInterfaceKit_getSensorCount(ifKit, &numSensors);

	//Change the sensitivity trigger of the sensors
	for (i = 0; i < numSensors; i++)
	{
		CPhidgetInterfaceKit_setSensorChangeTrigger(ifKit, i, 100);  //we'll just use 10 for fun
	}

	//read interface kit event data
	printf("Reading.....\n");

	//keep displaying interface kit data until user input is read
	printf("Press any key to go to next step\n");
	getchar();

	printf("Toggling Ratiometric....\n");

	CPhidgetInterfaceKit_setRatiometric(ifKit, 0);

	//read interface kit event data
	printf("Reading.....\n");

	return 0;
}

int stepper_simple()
{
	int result;
	__int64 curr_pos;
	const char *err;

	int stopped;
	int lastval, lastval2, lastval3, lastval4;
	

	CPhidgetStepperHandle stepper = 0;


	CPhidgetStepper_create(&stepper);


	CPhidget_set_OnAttach_Handler((CPhidgetHandle)stepper, AHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)stepper, DHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)stepper, EHandler, NULL);
	

	CPhidgetStepper_set_OnPositionChange_Handler(stepper, PCHandler, NULL);
	

	CPhidget_open((CPhidgetHandle)stepper, 398883); // --- Hard Code Serial Number ---
	
	printf("Waiting for MotorControl to be attached....");

	printf("Waiting for Phidget to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)stepper, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	DP(stepper);

	CPhidgetStepper_setAcceleration(stepper, 0, 20000);
	CPhidgetStepper_setVelocityLimit(stepper, 0, 11000);
	CPhidgetStepper_setCurrentLimit(stepper, 0, 1.2); // Current limit set to 1.2 mA

	CPhidgetAdvancedServo_setAcceleration(aservo, 0, 182857);
	CPhidgetAdvancedServo_setVelocityLimit(aservo, 0, 360);


	if (CPhidgetStepper_getCurrentPosition(stepper, 0, &curr_pos) == EPHIDGET_OK)
		printf("Motor: 0 > Current Position: %lld\n", curr_pos);

//================================================================================================
// Insertion Stage 1

	CPhidgetStepper_setCurrentPosition(stepper, 0, 0);
	CPhidgetStepper_setEngaged(stepper, 0, 1);


	printf("Press any key to continue\n");
	getchar();

	t = clock();

	cout << "===========================" << endl;
	cout << " Key in to begin Insertion: Stage 1" << endl;
	cout << "===========================" << endl;

	while (boolval == 0)
	{
		sys_z = -joy_value*new_conv;
		cout << sys_z << endl;
		CPhidgetStepper_setTargetPosition(stepper, 0, sys_z);
	}
	
	

	printf("Press any key to continue\n");

	getchar();
	
	cout << "===================================" << endl;
	cout << " Key in to begin retraction stage " << endl;
	cout << "===================================" << endl;

	char dummy;
	printf("Key in to continue\n");
	cin >> dummy;

	CPhidgetStepper_setTargetPosition(stepper, 0, 0);


	stopped = PFALSE;
	while (!stopped)
	{
		CPhidgetStepper_getStopped(stepper, 0, &stopped);
	}

	CPhidgetStepper_setEngaged(stepper, 0, 0);

	printf("Press any key to end\n");
	getchar();

	printf("Closing...\n");
	CPhidget_close((CPhidgetHandle)stepper);
	CPhidget_delete((CPhidgetHandle)stepper);
	t = clock() - t;
	return 0;
}

int main(int argc, char* argv[])
{
	/*myfile.open("Insertion_Test.txt", ios::in | ios::out | ios::app);
	myfile << "Insertion TESTING DATA" << endl;*/
	interfacekit_simple();
	stepper_simple();
	/*myfile << "==========================================================" << endl;
	myfile << "Stage 1: Insertion Depth: " << depth << endl;
	myfile << "Angle of Actuation:\t\t" << uip_initial << endl;
	myfile << "Stage 2: Insertion Depth: " << depth << endl;
	myfile << "Net Insertion Depth:: " << depth + depth2 << endl;
	myfile << "Elapsed Time: " << ((float)t) / CLOCKS_PER_SEC << endl;
	myfile << "==========================================================" << endl;*/

	return 0;
}


