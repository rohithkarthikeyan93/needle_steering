
#include "stdafx.h"
#include <sdkddkver.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <phidget21.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <tchar.h>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>




// Update: 07/19/2017

// Check how i1, i2 are increasing position variable
// Are they even needed?




// Check variable definitions - bool_val, joy_new,
// Compile and run!

using namespace std;

clock_t t;
int count_spl;
double start_val, distance_travelled, user_val, old_dutycycle;
int flag = 0;
int count_change = 0;
int i1=1,i2=1;


fstream myfile;

double user_z;
double new_conv = 640;
double sys_z, sys_z2;
int bool_val = 0;
int stop, flex;
double depth, depth2;
double Dist = 2;


double joy_value, joy_start, joy_new;



CPhidgetStepperHandle stepper;
CPhidgetInterfaceKitHandle ifkit;
CPhidgetAdvancedServoHandle aservo;


// ============ Call Back Events for Stepper ==================================

int CCONV AHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV DHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV EHandler(CPhidgetHandle stepper, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s\n", ErrorCode, Description);
	return 0;
}

int CCONV PCHandler(CPhidgetStepperHandle stepper, void *usrptr, int Index, __int64 Value)
{
		return 0;
}

int DP(CPhidgetStepperHandle phid)
{
	int serialNo, version, numMotors;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetStepper_getMotorCount(phid, &numMotors);


	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n# Motors: %d\n", serialNo, version, numMotors);

	return 0;
}
// ============================================================================


// ============ Call Back Events for IFKit ==================================

int CCONV attachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d attached!\n", name, serialNo);

	return 0;
}

int CCONV detachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d detached!\n", name, serialNo);

	return 0;
}

int CCONV errorHandler(CPhidgetHandle IFK, void *userptr, int ErrorCode, const char *unknown)
{
	printf("Error handled. %d - %s", ErrorCode, unknown);
	return 0;
}

int CCONV inputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Input: %d > State: %d\n", Index, State);
	bool_val = 0;
	return 0;
}

int CCONV outputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Output: %d > State: %d\n", Index, State);
	return 0;
}


void Insertion_Control()
{

	if (joy_new > joy_start)
	{
		/*sys_z = Dist*i1*new_conv;*/
		sys_z = Dist*new_conv;
		joy_start = joy_new;
		i1++;
		
	}

	else if (joy_new < joy_start)
	{
		/*sys_z = -Dist*i2*new_conv;*/
		sys_z = -Dist*new_conv;
		joy_start = joy_new;
		i2++;
	}

	else

	{
		sys_z = 0;
		joy_start = joy_new;
	}
	cout << "Target_position:" << sys_z << endl;
	
}
int CCONV sensorChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int Value)
{
	cout << "whoop" << endl;
	joy_new = Value;
	Insertion_Control();
	joy_start = joy_new;
	return 0;
}

int display_properties(CPhidgetInterfaceKitHandle phid)
{
	int serialNo, version, numInputs, numOutputs, numSensors, triggerVal, ratiometric, i;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetInterfaceKit_getInputCount(phid, &numInputs);
	CPhidgetInterfaceKit_getOutputCount(phid, &numOutputs);
	CPhidgetInterfaceKit_getSensorCount(phid, &numSensors);
	CPhidgetInterfaceKit_getRatiometric(phid, &ratiometric);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("# Digital Inputs: %d\n# Digital Outputs: %d\n", numInputs, numOutputs);
	printf("# Sensors: %d\n", numSensors);
	printf("Ratiometric: %d\n", ratiometric);

	for (i = 0; i < numSensors; i++)
	{
		CPhidgetInterfaceKit_getSensorChangeTrigger(phid, i, &triggerVal);

		printf("Sensor#: %d > Sensitivity Trigger: %d\n", i, triggerVal);
	}

	return 0;
}

// ============================================================================


int JOG_MODE_CONTROL()
{
	int result;
	__int64 curr_pos;
	const char *err;

	int stopped;
	int sensorValue;



	CPhidgetStepperHandle stepper = 0;
	CPhidgetInterfaceKitHandle ifKit = 0;

	CPhidgetStepper_create(&stepper);
	CPhidgetInterfaceKit_create(&ifKit);



	CPhidget_set_OnAttach_Handler((CPhidgetHandle)stepper, AHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)stepper, DHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)stepper, EHandler, NULL);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKit, attachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)ifKit, detachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)ifKit, errorHandler, NULL);

	CPhidgetInterfaceKit_set_OnSensorChange_Handler(ifKit, sensorChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnInputChange_Handler(ifKit, inputChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnOutputChange_Handler(ifKit, outputChangeHandler, NULL);

	CPhidgetStepper_set_OnPositionChange_Handler(stepper, PCHandler, NULL);


	CPhidget_open((CPhidgetHandle)stepper, 398883); // --- Hard Code Serial Number ---
	CPhidget_open((CPhidgetHandle)ifKit, 319699);


	printf("Waiting for interface kit to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)ifKit, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	printf("Waiting for Phidget to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)stepper, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	DP(stepper);
	display_properties(ifKit);

	CPhidgetStepper_setAcceleration(stepper, 0, 20000);
	CPhidgetStepper_setVelocityLimit(stepper, 0, 11000);
	CPhidgetStepper_setCurrentLimit(stepper, 0, 1.2); // Current limit set to 1.2 mA

	CPhidgetAdvancedServo_setAcceleration(aservo, 0, 182857);
	CPhidgetAdvancedServo_setVelocityLimit(aservo, 0, 360);


	if (CPhidgetStepper_getCurrentPosition(stepper, 0, &curr_pos) == EPHIDGET_OK)
		printf("Motor: 0 > Current Position: %lld\n", curr_pos);

	CPhidgetStepper_setCurrentPosition(stepper, 0, 0);
	CPhidgetStepper_setEngaged(stepper, 0, 1);


	joy_start = 0;
	bool_val = 1;
	
	do
	{	cout << "In the loop" << endl;
		cout << "***************************************************" << endl;
		cout << "Begin Jog-Mode Insertion" << endl;
		cout << "***************************************************" << endl;
		
		CPhidgetStepper_setTargetPosition(stepper, 0, sys_z);
		CPhidgetStepper_getStopped(stepper, 0, state);

	} while (bool_val ==1);
	
	stopped = PFALSE;

	while (!stopped)
	{
		CPhidgetStepper_getStopped(stepper, 0, &stopped);
	}

	CPhidgetStepper_setEngaged(stepper, 0, 0);

	printf("Press any key to end\n");
	getchar();

	printf("Closing...\n");
	CPhidget_close((CPhidgetHandle)stepper);
	CPhidget_delete((CPhidgetHandle)stepper);
	t = clock() - t;
	return 0;
}

int main(int argc, char* argv[])
{
	JOG_MODE_CONTROL();
	return 0;
}


