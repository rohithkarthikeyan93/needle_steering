#include "stdafx.h"
#include "TrackedObject.h"


TrackedObject::TrackedObject()
{
}

TrackedObject::TrackedObject(string name) {

	setType(name);

	if (name == "reference_point_y")
	{
		setHSVmin(Scalar(0, 56, 0));
		setHSVmax(Scalar(32, 256, 248));
		setColour(Scalar(0, 0, 0));
	}

	if (name == "yellow")
	{
		setHSVmin(Scalar(0, 0, 0));
		setHSVmax(Scalar(13, 235, 218));
		setColour(Scalar(0, 0, 0));
	}

	if (name == "red")
	{
		setHSVmin(Scalar(0, 65, 99));
		setHSVmax(Scalar(256, 256, 256));
		setColour(Scalar(0, 0, 0));
	}

	if (name == "green")
	{
		setHSVmin(Scalar(41, 96, 49));
		setHSVmax(Scalar(87, 256, 123));
		setColour(Scalar(0, 0, 0));
	}

}


TrackedObject::~TrackedObject()
{
}


int TrackedObject::getXPOS()
{
	return TrackedObject::xPos;
}

void TrackedObject::setXPOS(int x)
{
	TrackedObject::xPos = x;
	xPos = x;
}


int TrackedObject::getYPOS()
{
	return TrackedObject::yPos;
}

void TrackedObject::setYPOS(int y)
{
	TrackedObject::yPos = y;
	yPos = y;
}

Scalar TrackedObject::getHSVmin()
{
	return TrackedObject::HSVmin;
}

Scalar TrackedObject::getHSVmax()
{
	return TrackedObject::HSVmax;
}


void TrackedObject::setHSVmin(Scalar min)
{
	TrackedObject::HSVmin = min;
}

void TrackedObject::setHSVmax(Scalar max)
{
	TrackedObject::HSVmax = max;
}