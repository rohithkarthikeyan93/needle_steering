#include "NovintHandler.h"
#include "chai3d.h"

using namespace NovintHandler;
using namespace chai3d;
using namespace System::Threading;


novintHandler novintHandlerobj;

void novintHandler::updateHaptics(void)
{


	// initialize frequency counter
	novintHandlerobj.frequencyCounter.reset();
	cVector3d position;

	// simulation in now running
	novintHandlerobj.simulationRunning = true;
	novintHandlerobj.simulationFinished = false;

	// main haptic simulation loop
	while (novintHandlerobj.simulationRunning)
	{
		/////////////////////////////////////////////////////////////////////
		// READ HAPTIC DEVICE
		/////////////////////////////////////////////////////////////////////

		// read position 
		novintHandlerobj.hapticDevice->getPosition(position);

		// read user-switch status (button 0)
		bool button = false;
		novintHandlerobj.hapticDevice->getUserSwitch(0, button);

		// update frequency counter
		novintHandlerobj.frequencyCounter.signal(1);
	}

	// exit haptics thread
	novintHandlerobj.simulationFinished = true;
}

novintHandler novintHandler::initializeHapticThread(void)
{
	// create a haptic device handler
	novintHandlerobj.handler = new cHapticDeviceHandler();

	// get a handle to the first haptic device
	novintHandlerobj.handler->getDevice(novintHandlerobj.hapticDevice, 0);

	// open a connection to haptic device
	novintHandlerobj.hapticDevice->open();

	//// create a thread which starts the main haptics rendering loop
	novintHandlerobj.hapticsThread = new cThread();

	novintHandlerobj.hapticsThread->start(updateHaptics, CTHREAD_PRIORITY_HAPTICS);

	return novintHandlerobj;
}