#pragma once

#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "chai3d.h"
#include "TrackedObject.h"
#include "VisionHandler.h"


using namespace System::Windows::Forms;
using namespace chai3d;

struct valuesForRendering
{
	double avgx_array[700];
	double avgy_array[700];
	double avgz_array[700];
	int i1;
	int deleteoldpath;
	int pathpoints_num;

	double *topcamera_x, *topcamera_y, *sidecamera_x, *sidecamera_y, *topref_x, *topref_y, *sideref_x, *sideref_y;
	double *x_path, *y_path, *z_path;
	double *x_path_smooth, *y_path_smooth, *z_path_smooth;
	string cameraView;

	double topcamera_xstar[100], topcamera_ystar[100];
	double sidecamera_xstar[100], sidecamera_ystar[100];
	double openGL_x1[100],openGL_x2[100],openGL_x[100];
	double openGL_y[100], openGL_z[100];

	double obstacles_array[12]; // = { 2,3,2,3,9875,9875,9875,9875,9875,9875,9875,9875 };
	double start_position[3];
	double target_position[3];
	double entryradius;
	double targetradius;
	bool removeobstacle_1 = false;
	bool removeobstacle_2 = false;
	bool removeobstacle_3 = false;
	bool pathdone;

} renderingStruct;


int renderCount = 1;
double transparencylevel = 0.25;

namespace OpenGLRender
{
	cWorld* world;
	cCamera* camera;
	cSpotLight *light1;
	cDirectionalLight *light;
	//cPositionalLight *light;
	cShapeLine* line1;
	cShapeLine* line2;
	cShapeLine* line3;
	cMesh* bottompanel;
	cMultiSegment* needleSegment;
	cMultiSegment* generatedPath;
	cShapeSphere* obstacle1;
	cShapeSphere* obstacle2;
	cShapeSphere* obstacle3;
	cShapeSphere* entrySphere;
	cShapeSphere* targetSphere;

	cColorf color;
	double newpx1, newpy1, newpz1;
	double r1, r2, r3;
	double x1, y1, z1, x2, y2, z2, x3, y3, z3,x_start,y_start,z_start,x_target,y_target,z_target;

	public ref class COpenGL : public System::Windows::Forms::NativeWindow
	{
	public:
		COpenGL(System::Windows::Forms::TabPage ^ parentForm, GLsizei iWidth, GLsizei iHeight)
		{

			world = new cWorld();

			world->m_backgroundColor.setGrayLight();

			camera = new cCamera(world);
			camera->set(cVector3d(70, 70, 300.0),    // camera position (eye)
				cVector3d(70.0, 70.0, 0.0),    // lookat position (target)
				cVector3d(1.0, 0.0, 300.0));   // direction of the (up) vector
			world->addChild(camera);

			camera->setUseMultipassTransparency(true);

			light = new cDirectionalLight(world);
			// insert light source inside world
			world->addChild(light);
			// enable light source
			light->setEnabled(true);
			// define direction of light beam
			light->setDir(-1.0, 0.0, 0.0);

			line1 = new cShapeLine(cVector3d(0.0, 0.0, 0.0), cVector3d(20.0, 0.0, 0.0));
			line2 = new cShapeLine(cVector3d(0.0, 0.0, 0.0), cVector3d(0.0, 20.0, 0.0));
			line3 = new cShapeLine(cVector3d(0.0, 0.0, 0.0), cVector3d(0.0, 0.0, 20.0));
			needleSegment = new cMultiSegment();
			generatedPath = new cMultiSegment();
			obstacle1 = new cShapeSphere(r1);
			obstacle2 = new cShapeSphere(r2);
			obstacle3 = new cShapeSphere(r3);
			entrySphere = new cShapeSphere(renderingStruct.entryradius);
			targetSphere = new cShapeSphere(renderingStruct.targetradius);


			CreateParams^ cp = gcnew CreateParams;

			// Set the position on the form
			cp->X = 300;
			cp->Y = 20;
			cp->Height = iHeight;
			cp->Width = iWidth;

			// Specify the form as the parent.
			cp->Parent = parentForm->Handle;

			// Create as a child of the specified parent and make OpenGL compliant (no clipping)
			cp->Style = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;

			// Create the actual window
			this->CreateHandle(cp);

			m_hDC = GetDC((HWND)this->Handle.ToPointer());

			if (m_hDC)
			{
				MySetPixelFormat(m_hDC);
				ReSizeGLScene(iWidth, iHeight);
				InitGL();
			}

		}


		System::Void Render(valuesForRendering &renderingStruct)
		{
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glLoadIdentity();

			camera->renderView(800, 600);

			if (renderCount == 1)
			{
				world->addChild(line1);
				line1->m_colorPointA.setRed();
				line1->m_colorPointB.setRed();
				line1->setLineWidth(5);

				world->addChild(line2);
				line2->m_colorPointA.setGreen();
				line2->m_colorPointB.setGreen();
				line2->setLineWidth(5);

				world->addChild(line3);
				line3->m_colorPointA.setBlue();
				line3->m_colorPointB.setBlue();
				line3->setLineWidth(5);

				cMesh* toppanel = new cMesh();
				cCreatePanel(toppanel, 140, 140, 0, 8, cVector3d(70, 70, 60.0), cMatrix3d(cDegToRad(0), cDegToRad(0), cDegToRad(0), C_EULER_ORDER_XYZ));

				world->addChild(toppanel);
				toppanel->m_material->setOrange();
				toppanel->setUseCulling(true);
				toppanel->setUseTransparency(true);
				toppanel->setTransparencyLevel(transparencylevel);

				cMesh* bottompanel = new cMesh();
				cCreatePanel(bottompanel, 140, 140, 0, 8, cVector3d(70, 70, 0.0), cMatrix3d(cDegToRad(0), cDegToRad(0), cDegToRad(0), C_EULER_ORDER_XYZ));

				world->addChild(bottompanel);
				bottompanel->m_material->setRed();
				bottompanel->setUseCulling(true);
				bottompanel->setUseTransparency(true);
				bottompanel->setTransparencyLevel(transparencylevel);

				cMesh* frontpanel = new cMesh();
				cCreatePanel(frontpanel, 60, 140, 0, 8, cVector3d(140, 70, 30.0), cMatrix3d(cDegToRad(0), cDegToRad(90), cDegToRad(0), C_EULER_ORDER_XYZ));

				world->addChild(frontpanel);
				frontpanel->m_material->setBlack();
				frontpanel->setUseCulling(true);
				frontpanel->setUseTransparency(true);
				frontpanel->setTransparencyLevel(transparencylevel);

				cMesh* backpanel = new cMesh();
				cCreatePanel(backpanel, 60, 140, 0, 8, cVector3d(0, 70, 30.0), cMatrix3d(cDegToRad(0), cDegToRad(90), cDegToRad(0), C_EULER_ORDER_XYZ));

				world->addChild(backpanel);
				backpanel->m_material->setBrownMaroon();
				backpanel->setUseCulling(true);
				backpanel->setUseTransparency(true);
				backpanel->setTransparencyLevel(transparencylevel);

				cMesh* sidepanel1 = new cMesh();
				cCreatePanel(sidepanel1, 140, 60, 0, 8, cVector3d(70, 140, 30.0), cMatrix3d(cDegToRad(90), cDegToRad(0), cDegToRad(0), C_EULER_ORDER_XYZ));

				world->addChild(sidepanel1);
				sidepanel1->m_material->setBlueCornflower();
				sidepanel1->setUseCulling(true);
				sidepanel1->setUseTransparency(true);
				sidepanel1->setTransparencyLevel(transparencylevel);

				cMesh* sidepanel2 = new cMesh();
				cCreatePanel(sidepanel2, 140, 60, 0, 8, cVector3d(70, 0, 30.0), cMatrix3d(cDegToRad(90), cDegToRad(0), cDegToRad(0), C_EULER_ORDER_XYZ));

				world->addChild(sidepanel2);
				sidepanel2->m_material->setGreenChartreuse();
				sidepanel2->setUseCulling(true);
				sidepanel2->setUseTransparency(true);
				sidepanel2->setTransparencyLevel(transparencylevel);

				renderCount = 0;

			}

			//if (renderingStruct.removeobstacle_1 == false)
			{

				x1 = renderingStruct.obstacles_array[0];
				y1 = renderingStruct.obstacles_array[1];
				z1 = renderingStruct.obstacles_array[2];
				r1 = renderingStruct.obstacles_array[3];

				obstacle1->setRadius(r1);
				obstacle1->setLocalPos(x1, y1, z1);
				obstacle1->m_material->setRedCrimson();
				world->addChild(obstacle1);
			}


			//if (renderingStruct.removeobstacle_2 == false)
			{
				x2 = renderingStruct.obstacles_array[4];
				y2 = renderingStruct.obstacles_array[5];
				z2 = renderingStruct.obstacles_array[6];
				r2 = renderingStruct.obstacles_array[7];

				obstacle2->setRadius(r2);
				obstacle2->setLocalPos(x2, y2, z2);
				obstacle2->m_material->setRedCrimson();
				world->addChild(obstacle2);
			}

			//if (renderingStruct.removeobstacle_3 == false)
			{
				x3 = renderingStruct.obstacles_array[8];
				y3 = renderingStruct.obstacles_array[9];
				z3 = renderingStruct.obstacles_array[10];
				r3 = renderingStruct.obstacles_array[11];

				obstacle3->setRadius(r3);
				obstacle3->setLocalPos(x3, y3, z3);
				obstacle3->m_material->setRedCrimson();
				world->addChild(obstacle3);
			}



			x_start = renderingStruct.start_position[0];
			y_start = renderingStruct.start_position[1];
			z_start = renderingStruct.start_position[2];

			entrySphere->setRadius(renderingStruct.entryradius);
			entrySphere->setLocalPos(x_start, y_start, z_start);
			entrySphere->m_material->setGreenChartreuse();
			world->addChild(entrySphere);

			targetSphere->setRadius(renderingStruct.targetradius);
			x_target = renderingStruct.target_position[0];
			y_target = renderingStruct.target_position[1];
			z_target = renderingStruct.target_position[2];
			
			targetSphere->setLocalPos(x_target, y_target, z_target);
			targetSphere->m_material->setGreenChartreuse();
			world->addChild(targetSphere);


		needleSegment->clear();

		for (int j = 0; renderingStruct.openGL_x[j] != 0 && renderingStruct.openGL_y[j] != 0 && renderingStruct.openGL_z[j] && renderingStruct.openGL_x[j + 1] != 0 && renderingStruct.openGL_y[j + 1] != 0 && renderingStruct.openGL_z[j+1]!=0; j++)
		{


			double px0 = renderingStruct.openGL_x[j];
			double py0 = renderingStruct.openGL_y[j];
			double pz0 = renderingStruct.openGL_z[j];

			if (renderingStruct.openGL_x[j + 1] != 0 && renderingStruct.openGL_y[j + 1] != 0 && renderingStruct.openGL_z[j + 1]!=0)
				{
			            newpx1 =renderingStruct.openGL_x[j+1];
				     	newpy1 = renderingStruct.openGL_y[j+1];
				     	newpz1 = renderingStruct.openGL_z[j + 1];
				}

				// create vertex 0
				int index0 = needleSegment->newVertex(px0, py0, pz0);

				// create vertex 1
				int index1 = needleSegment->newVertex(newpx1, newpy1, newpz1);

				// create segment by connecting both vertices together
				needleSegment->newSegment(index0, index1);
		}

			// assign color properties
			color.setRed();

			needleSegment->setLineColor(color);

			// assign line width
			needleSegment->setLineWidth(6.0);

			// use display list for faster rendering
			needleSegment->setUseDisplayList(true);

			world->addChild(needleSegment);


			if (renderingStruct.cameraView == "frontview")
			{
				//world->clearAllChildren();
				//world->removeChild(line1);
				camera->set(cVector3d(-150, 70, 30.0),    // camera position (eye)
					cVector3d(0.0, 70.0, 30.0),    // lookat position (target)
					cVector3d(0, 0.0, 1.0));   // direction of the (up) vector
			}
			else if (renderingStruct.cameraView == "sideview")
			{
				camera->set(cVector3d(70, -150, 30.0),    // camera position (eye)
					cVector3d(70.0, 0.0, 30.0),    // lookat position (target)
					cVector3d(0.0, 0, 1.0));   // direction of the (up) vector

			}
			else if (renderingStruct.cameraView == "topview")
			{
				camera->set(cVector3d(70, 70, 300),    // camera position (eye)
					cVector3d(70.0, 70.0, 0.0),    // lookat position (target)
					cVector3d(1.0, 0.0, 300));   // direction of the (up) vector

			}
			else if (renderingStruct.cameraView == "isometricview")
			{
				camera->set(cVector3d(-100, -100, 90.0),    // camera position (eye)
					cVector3d(70.0, 70.0, 30.0),    // lookat position (target)
					cVector3d(1.0, 1.0, 1.0));   // direction of the (up) vector

			}


			if (renderingStruct.pathdone == true)
			{
				renderingStruct.pathdone = false;
				//double  test = renderingStruct.x_path[5];
				generatedPath->clear();

				for (int i = 0; i < renderingStruct.pathpoints_num-1; i++)
				{

					double px0 = renderingStruct.x_path[i];
					double py0 = renderingStruct.y_path[i];
					double pz0 = renderingStruct.z_path[i];

					double px1 = renderingStruct.x_path[i + 1];
					double py1 = renderingStruct.y_path[i + 1];
					double pz1 = renderingStruct.z_path[i + 1];

					// create vertex 0
					int index0 = generatedPath->newVertex(px0, py0, pz0);

					// create vertex 1
					int index1 = generatedPath->newVertex(px1, py1, pz1);

					// create segment by connecting both vertices together
					generatedPath->newSegment(index0, index1);

				}

				// assign color properties
				color.setBlack();

				generatedPath->setLineColor(color);

				// assign line width
				generatedPath->setLineWidth(4.0);

				// use display list for faster rendering
				generatedPath->setUseDisplayList(true);

				world->addChild(generatedPath);

			}

		}

		System::Void SwapOpenGLBuffers(System::Void)
		{
			SwapBuffers(m_hDC);
		}

	private:
		HDC m_hDC;
		HGLRC m_hglrc;

	protected:
		~COpenGL(System::Void)
		{
			this->DestroyHandle();
		}

		GLint MySetPixelFormat(HDC hdc)
		{
			static	PIXELFORMATDESCRIPTOR pfd =				// pfd Tells Windows How We Want Things To Be
			{
				sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
				1,											// Version Number
				PFD_DRAW_TO_WINDOW |						// Format Must Support Window
				PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
				PFD_DOUBLEBUFFER,							// Must Support Double Buffering
				PFD_TYPE_RGBA,								// Request An RGBA Format
				16,										// Select Our Color Depth
				0, 0, 0, 0, 0, 0,							// Color Bits Ignored
				0,											// No Alpha Buffer
				0,											// Shift Bit Ignored
				0,											// No Accumulation Buffer
				0, 0, 0, 0,									// Accumulation Bits Ignored
				16,											// 16Bit Z-Buffer (Depth Buffer)  
				0,											// No Stencil Buffer
				0,											// No Auxiliary Buffer
				PFD_MAIN_PLANE,								// Main Drawing Layer
				0,											// Reserved
				0, 0, 0										// Layer Masks Ignored
			};

			GLint  iPixelFormat;

			// get the device context's best, available pixel format match 
			if ((iPixelFormat = ChoosePixelFormat(hdc, &pfd)) == 0)
			{
				MessageBox::Show("ChoosePixelFormat Failed");
				return 0;
			}

			// make that match the device context's current pixel format 
			if (SetPixelFormat(hdc, iPixelFormat, &pfd) == FALSE)
			{
				MessageBox::Show("SetPixelFormat Failed");
				return 0;
			}

			if ((m_hglrc = wglCreateContext(m_hDC)) == NULL)
			{
				MessageBox::Show("wglCreateContext Failed");
				return 0;
			}

			if ((wglMakeCurrent(m_hDC, m_hglrc)) == NULL)
			{
				MessageBox::Show("wglMakeCurrent Failed");
				return 0;
			}


			return 1;
		}

		bool InitGL(GLvoid)										// All setup for opengl goes here
		{

			glShadeModel(GL_SMOOTH);							// Enable smooth shading
			glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Black background
			glClearDepth(1.0f);									// Depth buffer setup
			glEnable(GL_DEPTH_TEST);							// Enables depth testing
			glDepthFunc(GL_LEQUAL);								// The type of depth testing to do
			glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really nice perspective calculations
			return TRUE;										// Initialisation went ok
		}

		GLvoid ReSizeGLScene(GLsizei width, GLsizei height)		// Resize and initialise the gl window
		{
			if (height == 0)										// Prevent A Divide By Zero By
			{
				height = 1;										// Making Height Equal One
			}

			glViewport(0, 0, width, height);						// Reset The Current Viewport

			glMatrixMode(GL_PROJECTION);						// Select The Projection Matrix
			glLoadIdentity();									// Reset The Projection Matrix

																// Calculate The Aspect Ratio Of The Window
			gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

			glMatrixMode(GL_MODELVIEW);							// Select The Modelview Matrix
			glLoadIdentity();									// Reset The Modelview Matrix
		}
	};
}
