#include "PhidgetHandler.h"

using namespace PhidgetHandler;

phidgetHandler phidgetHandlerobj;


void CCONV phidgetHandler::onAttachHandler(PhidgetHandle phid, void *ctx) {
	PhidgetReturnCode res;
	int hubPort;
	int channel;
	int serial;

	res = Phidget_getDeviceSerialNumber(phid, &serial);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to get device serial number\n");
		return;
	}

	res = Phidget_getChannel(phid, &channel);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to get channel number\n");
		return;
	}

	res = Phidget_getHubPort(phid, &hubPort);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to get hub port\n");
		hubPort = -1;
	}

	if (hubPort == -1)
		printf("channel %d on device %d attached\n", channel, serial);
	else
		printf("channel %d on device %d hub port %d attached\n", channel, serial, hubPort);
}

void CCONV phidgetHandler::onDetachHandler(PhidgetHandle phid, void *ctx) {
	PhidgetReturnCode res;
	int hubPort;
	int channel;
	int serial;

	res = Phidget_getDeviceSerialNumber(phid, &serial);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to get device serial number\n");
		return;
	}

	res = Phidget_getChannel(phid, &channel);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to get channel number\n");
		return;
	}

	res = Phidget_getHubPort(phid, &hubPort);
	if (res != EPHIDGET_OK)
		hubPort = -1;

	if (hubPort != -1)
		printf("channel %d on device %d detached\n", channel, serial);
	else
		printf("channel %d on device %d hub port %d detached\n", channel, hubPort, serial);
}

void CCONV phidgetHandler::errorHandler(PhidgetHandle phid, void *ctx, Phidget_ErrorEventCode errorCode, const char *errorString) {

	fprintf(stderr, "Error: %s (%d)\n", errorString, errorCode);
}

void CCONV phidgetHandler::onPositionChangeHandler(PhidgetStepperHandle ch, void *ctx, double position) {

	printf("Position Changed: %.0lf\n", position);
}

void CCONV phidgetHandler::onVelocityChangeHandler(PhidgetStepperHandle ch, void *ctx, double velocity) {

	printf("Velocity Changed: %.0lf\n", velocity);
}

void CCONV phidgetHandler::onStoppedHandler(PhidgetStepperHandle ch, void *ctx) {

	printf("Stopped!\n");
}


/*
* Creates and initializes the channel.
*/
PhidgetReturnCode CCONV phidgetHandler::initChannel(PhidgetHandle ch) {
	PhidgetReturnCode res;

	res = Phidget_setOnAttachHandler(ch, onAttachHandler, NULL);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to assign on attach handler\n");
		return (res);
	}

	res = Phidget_setOnDetachHandler(ch, onDetachHandler, NULL);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to assign on detach handler\n");
		return (res);
	}

	res = Phidget_setOnErrorHandler(ch, errorHandler, NULL);
	if (res != EPHIDGET_OK) {
		fprintf(stderr, "failed to assign on error handler\n");
		return (res);
	}

	return (EPHIDGET_OK);
}

phidgetHandler phidgetHandler::initializeStepper()
{

	PhidgetLog_enable(PHIDGET_LOG_INFO, NULL);


	phidgetHandlerobj.res = PhidgetStepper_create(&phidgetHandlerobj.ch);
	if (phidgetHandlerobj.res != EPHIDGET_OK) {

		//materialLabel2->Text = "failed to create stepper channel";

		exit(1);
	}

	phidgetHandlerobj.res = initChannel((PhidgetHandle)phidgetHandlerobj.ch);
	if (phidgetHandlerobj.res != EPHIDGET_OK) {
		Phidget_getErrorDescription(phidgetHandlerobj.res, &phidgetHandlerobj.errs);
		fprintf(stderr, "failed to initialize channel:%s\n", phidgetHandlerobj.errs);
		exit(1);
	}

	phidgetHandlerobj.res = PhidgetStepper_setOnPositionChangeHandler(phidgetHandlerobj.ch, onPositionChangeHandler, NULL);
	if (phidgetHandlerobj.res != EPHIDGET_OK) {
		Phidget_getErrorDescription(phidgetHandlerobj.res, &phidgetHandlerobj.errs);
		//fprintf(stderr, "failed to set position change handler: %s\n", errs);
		//goto done;
	}


	phidgetHandlerobj.res = PhidgetStepper_setOnVelocityChangeHandler(phidgetHandlerobj.ch, onVelocityChangeHandler, NULL);
	if (phidgetHandlerobj.res != EPHIDGET_OK) {
		Phidget_getErrorDescription(phidgetHandlerobj.res, &phidgetHandlerobj.errs);
		//fprintf(stderr, "failed to set velocity change handler: %s\n", errs);
		//goto done;
	}

	phidgetHandlerobj.res = PhidgetStepper_setOnStoppedHandler(phidgetHandlerobj.ch, onStoppedHandler, NULL);
	if (phidgetHandlerobj.res != EPHIDGET_OK) {
		Phidget_getErrorDescription(phidgetHandlerobj.res, &phidgetHandlerobj.errs);
		//fprintf(stderr, "failed to set stopped handler: %s\n", errs);
		//goto done;
	}

	phidgetHandlerobj.res = Phidget_openWaitForAttachment((PhidgetHandle)phidgetHandlerobj.ch, 5000);
	if (phidgetHandlerobj.res != EPHIDGET_OK) {
		if (phidgetHandlerobj.res == EPHIDGET_TIMEOUT) {
			//printf("Channel did not attach after 5 seconds: please check that the device is attached\n");
		}
		else {
			Phidget_getErrorDescription(phidgetHandlerobj.res, &phidgetHandlerobj.errs);
			//fprintf(stderr, "failed to open channel:%s\n", errs);
		}
		//goto done;
	}

	PhidgetStepper_setEngaged(phidgetHandlerobj.ch, 1);

	return phidgetHandlerobj;
}