#include "VisionHandler.h"
#include "TrackedObject.h"
#include <opencv\highgui.h>
#include <opencv\cv.h>


using namespace VisionHandler;
using namespace cv;

double x_test[20];
double y_test[20];
double xref_test[20];
double yref_test[20];

void swap(double *xp, double *yp)
{
	int temp = *xp;
	*xp = *yp;
	*yp = temp;
}

void bubbleSortDesc(double arr[], double arr2[], double n)
{
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; i++)
	{
		swapped = false;
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr2[j] < arr2[j + 1])
			{
				swap(&arr2[j], &arr2[j + 1]);
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}

		// IF no two elements were swapped by inner loop, then break
		if (swapped == false)
			break;
	}
}

void bubbleSortAsc(double arr[], double arr2[], double n)
{
	int i, j;
	bool swapped;
	for (i = 0; i < n - 1; i++)
	{
		swapped = false;
		for (j = 0; j < n - i - 1; j++)
		{
			if (arr2[j] > arr2[j + 1])
			{
				swap(&arr2[j], &arr2[j + 1]);
				swap(&arr[j], &arr[j + 1]);
				swapped = true;
			}
		}

		// IF no two elements were swapped by inner loop, then break
		if (swapped == false)
			break;
	}
}

void visionHandler::drawObject(visionHandler &visionHandlerobj, vector<TrackedObject> trackedObjects, Mat &frame, double *&x, double *&y,int sortingorder) // function to show indication of the colours tracked
{
	int n = trackedObjects.size();

	visionHandlerobj.num_trackedobjects = n;

	x = new double[n];
	y = new double[n];


	for (int i = 0; i<n; i++)
	{
		x[i] = 0;
		y[i] = 0;
		x_test[i] = 0;
		y_test[i] = 0;
	}

	for (int i = 0; i<trackedObjects.size() && trackedObjects.size()>0; i++) {

		cv::circle(frame, cv::Point(trackedObjects.at(i).getXPOS(), trackedObjects.at(i).getYPOS()), 10, cv::Scalar(0, 0, 255));
		cv::putText(frame, to_string(trackedObjects.at(i).getXPOS()) + " , " + to_string(trackedObjects.at(i).getYPOS()), cv::Point(trackedObjects.at(i).getXPOS(), trackedObjects.at(i).getYPOS() + 20), 1, 1, Scalar(0, 255, 0));
		cv::putText(frame, trackedObjects.at(i).getType(), cv::Point(trackedObjects.at(i).getXPOS(), trackedObjects.at(i).getYPOS() - 30), 1, 2, trackedObjects.at(i).getColour());

		x[i] = trackedObjects.at(i).getXPOS();
		y[i] = trackedObjects.at(i).getYPOS();

	}

	if (sortingorder == 1)  // points captured in top view are arranged in descending order
	{
		bubbleSortDesc(x, y, n);
		for (int i = 0; i < trackedObjects.size() && trackedObjects.size()>0; i++)
		{
			 x_test[i] = x[i];
			 y_test[i] = y[i];
		}
		
	}
	else if (sortingorder == 2) // points captured in side view are arranged in ascending order
	{
		bubbleSortAsc(y, x, n);

		//for (int i = 0; i < trackedObjects.size() && trackedObjects.size()>0; i++)
		//{
		//	x_test[i] = x[i];
		//	y_test[i] = y[i];
		//}

	}
	

}

void visionHandler::drawObject( vector<TrackedObject> trackedObjects, Mat &frame, double *&x, double *&y) // overloaded function used while storing reference points
{
	int n = trackedObjects.size();

	x = new double[n];
	y = new double[n];

	for (int i = 0; i<n; i++)
	{
		x[i] = 0;
		y[i] = 0;
	}

	for (int i = 0; i<trackedObjects.size() && trackedObjects.size()>0; i++) {

		cv::circle(frame, cv::Point(trackedObjects.at(i).getXPOS(), trackedObjects.at(i).getYPOS()), 10, cv::Scalar(0, 0, 255));
		cv::putText(frame, to_string(trackedObjects.at(i).getXPOS()) + " , " + to_string(trackedObjects.at(i).getYPOS()), cv::Point(trackedObjects.at(i).getXPOS(), trackedObjects.at(i).getYPOS() + 20), 1, 1, Scalar(0, 255, 0));
		cv::putText(frame, trackedObjects.at(i).getType(), cv::Point(trackedObjects.at(i).getXPOS(), trackedObjects.at(i).getYPOS() - 30), 1, 2, trackedObjects.at(i).getColour());

		x[i] = trackedObjects.at(i).getXPOS();
		
		y[i] = trackedObjects.at(i).getYPOS();
		
	}

	bubbleSortDesc(x, y, n);

	//for (int i = 0; i < trackedObjects.size() && trackedObjects.size()>0; i++)
	//{
	//	xref_test[i] = x[i];
	//	yref_test[i] = y[i];
	//}

}

void  visionHandler::morphOps(Mat &thresh)
{
	//create structuring element that will be used to "dilate" and "erode" image.
	//the element chosen here is a 3px by 3px rectangle

	Mat erodeElement = getStructuringElement(MORPH_RECT, cv::Size(4, 4));
	//dilate with larger element so make sure object is nicely visible
	Mat dilateElement = getStructuringElement(MORPH_RECT, cv::Size(10, 10));

	erode(thresh, thresh, erodeElement);
	erode(thresh, thresh, erodeElement);

	imshow("Erode", thresh);

	dilate(thresh, thresh, dilateElement);
	dilate(thresh, thresh, dilateElement);

	//imshow("dilate", thresh);
}

void visionHandler::trackFilteredObject(visionHandler &visionHandlerobj, Mat threshold, Mat HSV, Mat &cameraFeed, double *x, double *y, double *ref_x, double *ref_y) // function for tracking while calibration mode is on
{
	vector<TrackedObject> points_on_needle;
	Mat temp;
	threshold.copyTo(temp);
	//these two vectors needed for output of findContours
	vector< vector<cv::Point> > contours;
	vector<Vec4i> hierarchy;
	//find contours of filtered image using openCV findContours function
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	//use moments method to find our filtered object
	double refArea = 0;
	bool objectFound = true;
	if (hierarchy.size() > 0) {
		int numObjects = hierarchy.size();
		//if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
		if (numObjects<visionHandlerobj.MAX_NUM_OBJECTS) {
			for (int index = 0; index >= 0; index = hierarchy[index][0]) {

				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;

				//if the area is less than 20 px by 20px then it is probably just noise
				//if the area is the same as the 3/2 of the image size, probably just a bad filter
				//we only want the object with the largest area so we safe a reference area each
				//iteration and compare it to the area in the next iteration.
				if (area>visionHandlerobj.MIN_OBJECT_AREA) {

					TrackedObject trackedObject;

					trackedObject.setXPOS(moment.m10 / area);
					trackedObject.setYPOS(moment.m01 / area);

					if (1)
					{
						points_on_needle.push_back(trackedObject);

						objectFound = true;

					}

				}
				else objectFound = false;


			}
			//let user know you found an object
			if (objectFound == true) {
				//draw object location on screen
				drawObject(points_on_needle, cameraFeed, x, y);
			}

		}
		else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", cv::Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}

void  visionHandler::trackFilteredObject(visionHandler &visionHandlerobj, TrackedObject thetrackedObject, Mat threshold, Mat HSV, Mat &cameraFeed, double *&x, double *&y, double *&ref_x, double *&ref_y,int sortingorder) // overloaded function for tracking while calibration mode is off
{
	vector<TrackedObject> points_on_needle;
	vector<TrackedObject> reference_points;
	Mat temp;
	threshold.copyTo(temp);
	//these two vectors needed for output of findContours
	vector< vector<cv::Point> > contours;
	vector<Vec4i> hierarchy;
	//find contours of filtered image using openCV findContours function
	findContours(temp, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	//use moments method to find our filtered object
	double refArea = 0;
	bool objectFound = false;
	if (hierarchy.size() > 0) {
		int numObjects = hierarchy.size();
		//if number of objects greater than MAX_NUM_OBJECTS we have a noisy filter
		if (numObjects<visionHandlerobj.MAX_NUM_OBJECTS) {
			for (int index = 0; index >= 0; index = hierarchy[index][0]) {

				Moments moment = moments((cv::Mat)contours[index]);
				double area = moment.m00;

				//if the area is less than 20 px by 20px then it is probably just noise
				//if the area is the same as the 3/2 of the image size, probably just a bad filter
				//we only want the object with the largest area so we safe a reference area each
				//iteration and compare it to the area in the next iteration.
				if (area>visionHandlerobj.MIN_OBJECT_AREA) {

					TrackedObject trackedObject;

					trackedObject.setXPOS(moment.m10 / area);
					trackedObject.setYPOS(moment.m01 / area);

					trackedObject.setType(thetrackedObject.getType());
					trackedObject.setColour(thetrackedObject.getColour());

					if (trackedObject.getType() == "green")
					{

						reference_points.push_back(trackedObject);
						visionHandlerobj.tracked_reference_points = true;
						if (area > 15 * 15)
							objectFound = true;
						else
							objectFound = false;
					}

					else
					{
						points_on_needle.push_back(trackedObject);
						visionHandlerobj.tracked_reference_points = false;

						objectFound = true;

					}

				}
				else objectFound = false;


			}
			//let user know you found an object
			if (objectFound == true && visionHandlerobj.tracked_reference_points == false) {
				//draw object location on screen
				drawObject(visionHandlerobj,points_on_needle, cameraFeed, x, y,sortingorder);
			}
			else if (objectFound == true && visionHandlerobj.tracked_reference_points == true) {
				//draw object location on screen
				drawObject(reference_points, cameraFeed, ref_x, ref_y);
			}

		}
		//else putText(cameraFeed, "TOO MUCH NOISE! ADJUST FILTER", Point(0, 50), 1, 2, Scalar(0, 0, 255), 2);
	}
}
