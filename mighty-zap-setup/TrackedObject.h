#pragma once
#include <string>
#include <opencv\cv.h>
#include <opencv2\highgui\highgui.hpp>

using namespace std;
using namespace cv;
class TrackedObject
{
public:
	TrackedObject(void);
	~TrackedObject(void);

	TrackedObject(string name);

	int getXPOS();
	void setXPOS(int x);
	int getYPOS();
	void setYPOS(int y);

	Scalar getHSVmin();
	Scalar getHSVmax();

	void setHSVmin(Scalar min);
	void setHSVmax(Scalar max);

	string getType() { return type; }
	void setType(string t) { type = t; }

	Scalar getColour() {
		return Colour;
	}

	void setColour(Scalar c) {

		Colour = c;
	}

private:
	int xPos, yPos;
	string type;
	Scalar HSVmin, HSVmax;
	Scalar Colour;

};