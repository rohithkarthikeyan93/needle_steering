#pragma once
#include <opencv\highgui.h>
#include <opencv\cv.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include "TrackedObject.h"

using namespace std;
using namespace cv;


namespace VisionHandler {

	class visionHandler {

	public:

		const int FRAME_WIDTH = 1271;
		const int FRAME_HEIGHT = 410;

		int num_trackedobjects;

		//max number of objects to be detected in frame
		const int MAX_NUM_OBJECTS = 20;
		//minimum and maximum object area
		int MIN_OBJECT_AREA = 8 * 8;
		const int MAX_OBJECT_AREA = FRAME_HEIGHT * FRAME_WIDTH / 1.5;

		const string windowName2 = "Thresholded Image";

		Mat inrImg, hsvImg, srcImg, overlay, threshold, resizedsrcImg;


		bool trackObjects = true;
		bool useMorphOps = true;

		bool trackbarCreated = false;

		bool calibrationMode = false;

		bool referenceMode = false;

		bool tracked_reference_points;

		VideoCapture capture;

		void drawObject(visionHandler &visionHandlerobj, vector<TrackedObject> trackedObjects, Mat &frame, double *&x, double *&y,int sortingorder);

		void drawObject(vector<TrackedObject> trackedObjects, Mat &frame, double *&x, double *&y);

		void morphOps(Mat &thresh);

		void trackFilteredObject(visionHandler &visionHandlerobj,Mat threshold, Mat HSV, Mat &cameraFeed, double *x, double *y, double *ref_x, double *ref_y);

		void trackFilteredObject(visionHandler &visionHandlerobj,TrackedObject thetrackedObject, Mat threshold, Mat HSV, Mat &cameraFeed, double *&x, double *&y, double *&ref_x, double *&ref_y,int sortingorder);



	};

}