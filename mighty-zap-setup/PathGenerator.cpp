#include "PathGenerator.h"
#include <math.h>

using namespace PathGenerator;

	
double pathGenerator:: function(double x, double y, double z, double goal[], double obstacles[],int size)

	{

	int i;

	double margin = 4;

	for (i = 1; i<12; i++)

	{

		if (obstacles[i] == 9875)

			break;

	}

	double num_obstacles = i / 4;



		double f = 0.1*(pow(x - goal[0], 2) + pow(y - goal[1], 2) + pow(z - goal[2], 2));

		f = f + (-150)*step(num_obstacles - 1)*step(obstacles[3] + margin - sqrt(pow(x - obstacles[0], 2) + pow(y - obstacles[1], 2) + pow(z - obstacles[2], 2)))*(sqrt(pow(x - obstacles[0], 2) + pow(y - obstacles[1], 2) + pow(z - obstacles[2], 2)));

		f = f + (-150)*step(num_obstacles - 2)*step(obstacles[7] + margin - sqrt(pow(x - obstacles[4], 2) + pow(y - obstacles[5], 2) + pow(z - obstacles[6], 2)))*(sqrt(pow(x - obstacles[4], 2) + pow(y - obstacles[5], 2) + pow(z - obstacles[6], 2)));

		f = f + (-150)*step(num_obstacles - 3)*step(obstacles[11] + margin - sqrt(pow(x - obstacles[8], 2) + pow(y - obstacles[9], 2) + pow(z - obstacles[10], 2)))*(sqrt(pow(x - obstacles[8], 2) + pow(y - obstacles[9], 2) + pow(z - obstacles[10], 2)));


	
	return(f);

	}

double pathGenerator::step(double x)

{

	if (x<0)

		return(0);

	else

		return(1);

}
