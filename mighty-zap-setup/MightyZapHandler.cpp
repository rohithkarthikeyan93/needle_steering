#include "MightyZapHandler.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>


#define PORT_ON 1
#define PORT_OFF 0

using namespace MightyZapHandler;
using namespace std;


void mightyZapHandler::initializeMightyZapMotors(mightyZapHandler &mightyZapHandlerobj)
{

	mightyZapHandlerobj.mightyzap = new MightyZap(0);


	if (mightyZapHandlerobj.portOpenState == PORT_ON)
	{
		mightyZapHandlerobj.mightyzap->CloseMightyZap();

		mightyZapHandlerobj.portOpenState = PORT_OFF;

	}
	else
	{
		mightyZapHandlerobj.mightyzap->OpenMightyZap(mightyZapHandlerobj.p_name, CBR_57600);

		mightyZapHandlerobj.portOpenState = PORT_ON;
	}

	/// 1. Force limiting

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id1_str), mightyZapHandlerobj.addr_force, 2, mightyZapHandlerobj.limitingForce);

	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id3_str), mightyZapHandlerobj.addr_force, 2, mightyZapHandlerobj.limitingForce);

	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id2_str), mightyZapHandlerobj.addr_force, 2, mightyZapHandlerobj.limitingForce);

	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id4_str), mightyZapHandlerobj.addr_force, 2, mightyZapHandlerobj.limitingForce);

	Sleep(10);



	// 3. Neutral Positioning of motors

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id1_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor1_neutral);
	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id3_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor3_neutral);
	Sleep(10);

	///4 . Checking reached position
	while (1)
	{
		mightyZapHandlerobj.position_1 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id1_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(2);
		mightyZapHandlerobj.position_3 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id3_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(40);

		mightyZapHandlerobj.position_1_after40ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id1_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(2);
		mightyZapHandlerobj.position_3_after40ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id3_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(40);

		mightyZapHandlerobj.position_1_after80ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id1_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(2);
		mightyZapHandlerobj.position_3_after80ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id3_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(40);

		mightyZapHandlerobj.force_1 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id1_str), mightyZapHandlerobj.addr_force, 2);
		Sleep(10);
		mightyZapHandlerobj.force_3 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id3_str), mightyZapHandlerobj.addr_force, 2);
		Sleep(10);

		if ((abs(mightyZapHandlerobj.position_1 - mightyZapHandlerobj.position_1_after40ms) + abs(mightyZapHandlerobj.position_1 - mightyZapHandlerobj.position_1_after80ms) < 2) && (abs(mightyZapHandlerobj.position_3 - mightyZapHandlerobj.position_3_after40ms) + abs(mightyZapHandlerobj.position_3 - mightyZapHandlerobj.position_3_after80ms) < 2) && (mightyZapHandlerobj.force_1 != 0) && (mightyZapHandlerobj.force_3 != 0))
		{
			break;
		}
		if ((mightyZapHandlerobj.force_1 == 0) || (mightyZapHandlerobj.force_3 == 0))
		{
			cout << "Error! Motors force is zero" << endl;
			break;
		}
	}

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id2_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor2_neutral);
	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id4_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor4_neutral);
	Sleep(10);


	while (1)
	{
		mightyZapHandlerobj.position_1 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id2_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(2);
		mightyZapHandlerobj.position_3 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id4_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(40);

		mightyZapHandlerobj.position_1_after40ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id2_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(2);
		mightyZapHandlerobj.position_3_after40ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id4_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(40);

		mightyZapHandlerobj.position_1_after80ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id2_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(2);
		mightyZapHandlerobj.position_3_after80ms = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id4_str), mightyZapHandlerobj.addr_presentposition, 2);
		Sleep(40);

		mightyZapHandlerobj.force_1 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id2_str), mightyZapHandlerobj.addr_force, 2);
		Sleep(10);
		mightyZapHandlerobj.force_3 = mightyZapHandlerobj.mightyzap->read_Addr(_ttoi(mightyZapHandlerobj.id4_str), mightyZapHandlerobj.addr_force, 2);
		Sleep(10);

		if ((abs(mightyZapHandlerobj.position_1 - mightyZapHandlerobj.position_1_after40ms) + abs(mightyZapHandlerobj.position_1 - mightyZapHandlerobj.position_1_after80ms) < 2) && (abs(mightyZapHandlerobj.position_3 - mightyZapHandlerobj.position_3_after40ms) + abs(mightyZapHandlerobj.position_3 - mightyZapHandlerobj.position_3_after80ms) < 2) && (mightyZapHandlerobj.force_1 != 0) && (mightyZapHandlerobj.force_3 != 0))
		{
			break;
		}
		if ((mightyZapHandlerobj.force_1 == 0) || (mightyZapHandlerobj.force_3 == 0))
		{
			cout << "Error! Motors force is zero" << endl;
			break;
		}
	}

}

void mightyZapHandler::mapNovintToNeedleTip(double novint_calibrated_z, double novint_calibrated_y, mightyZapHandler &mightyZapHandlerobj) // function that maps the novint cursor's movement to the motor positioning to bend the flexure tip
{

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id1_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor1_neutral - novint_calibrated_z);

	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id3_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor3_neutral + novint_calibrated_z);

	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id2_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor2_neutral - novint_calibrated_y);

	Sleep(10);

	mightyZapHandlerobj.mightyzap->write_Addr(_ttoi(mightyZapHandlerobj.id4_str), mightyZapHandlerobj.addr_goalposition, 2, mightyZapHandlerobj.motor4_neutral + novint_calibrated_y);

	Sleep(10);

}