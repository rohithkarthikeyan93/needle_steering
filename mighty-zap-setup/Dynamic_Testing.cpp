

#include "stdafx.h"
#include <stdio.h>
#include <phidget21.h>
#include <stdlib.h>
#include <iostream>
#include<math.h>
#include<windows.h>
#include<time.h>
#include<iostream>
#include<fstream>
#include<string>


using namespace std;
fstream myfile;

//Code for Motor Position Control with PID using encoder feedback data
clock_t t;
double uip,time_val;							//User Input for Insertion Depth
int global_pose = 0;				//Encoder Position Value
double kp = 1;						//Proportional Gain
double kd = 7;						//Derivative Gain
double ki = 100;					//Intergral Gain
double dutycycle;					//Dutycycle for motor
int maxoutput = 25;					//Maximum duty cycle
double deadband = 0.4;				//Error Margin
double dt = 0.008;					//Encoder Update Rate
double uipmod,uip_initial;			//User Input Scaled to Encoder Values [includes Gear Ratio, Tick Rate,etc.]
double error,angle_error;
double derivative, integral;
double errorlast = 0;
double velocity;
double conv = 1227.122;				// Conversion Factor for encoding
double pi = 3.14159265359;
double counter = 0;
int iter;
int x = 0;
int CCONV AttachHandler(CPhidgetHandle MC, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(MC, &name);
	CPhidget_getSerialNumber(MC, &serialNo);
	printf("%s %10d attached!\n", name, serialNo);

	return 0;
}

int CCONV DetachHandler(CPhidgetHandle MC, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(MC, &name);
	CPhidget_getSerialNumber(MC, &serialNo);
	printf("%s %10d detached!\n", name, serialNo);
	return 0;
}

int CCONV ErrorHandler(CPhidgetHandle MC, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s\n", ErrorCode, Description);
	return 0;
}


int CCONV InputChangeHandler(CPhidgetMotorControlHandle MC, void *usrptr, int Index, int State)
{
	printf("Input %d > State: %d\n", Index, State);
	return 0;
}

int CCONV VelocityChangeHandler(CPhidgetMotorControlHandle MC, void *usrptr, int Index, double Value)
{
	velocity = Value;

	return 0;
}

int CCONV CurrentChangeHandler(CPhidgetMotorControlHandle MC, void *usrptr, int Index, double Value)
{

	return 0;

}

//PID Loop accepts User Input (UIP) and sets motor DutyCycle accodingly

void PID()
{
	uipmod = uip*conv; // Conversion Factor
	errorlast = error;
	error = uipmod - global_pose;


	if (abs(error) <= deadband)
	{
		dutycycle = 0;
		error = 0;
	}
	else {
		dutycycle = (kp*error) + (kd*derivative) + (ki*integral);
	}

	if (dutycycle >= maxoutput)
		dutycycle = maxoutput;
	else if (dutycycle <= -maxoutput)
		dutycycle = -maxoutput;
	else

		integral += error*dt;

	derivative = (error - errorlast) / dt;

}



int display_properties(CPhidgetMotorControlHandle phid)
{
	int serialNo, version, numInputs, numMotors;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetMotorControl_getInputCount(phid, &numInputs);
	CPhidgetMotorControl_getMotorCount(phid, &numMotors);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("# Inputs: %d\n# Motors: %d\n", numInputs, numMotors);

	return 0;
}








int CCONV attachHandler(CPhidgetHandle ENC, void *userptr)
{
	int serialNo;
	CPhidget_DeviceID deviceID;
	int i, inputcount;

	CPhidget_getSerialNumber(ENC, &serialNo);

	//Retrieve the device ID and number of encoders so that we can set the enables if needed
	CPhidget_getDeviceID(ENC, &deviceID);
	CPhidgetEncoder_getEncoderCount((CPhidgetEncoderHandle)ENC, &inputcount);
	printf("Encoder %10d attached! \n", serialNo);

	//the 1047 requires enabling of the encoder inputs, so enable them if this is a 1047    
	if (deviceID == PHIDID_ENCODER_HS_4ENCODER_4INPUT)
	{
		printf("Encoder requires Enable. Enabling inputs....\n");
		for (i = 0; i < inputcount; i++)
			CPhidgetEncoder_setEnabled((CPhidgetEncoderHandle)ENC, i, 1);
	}
	return 0;
}


int CCONV detachHandler(CPhidgetHandle ENC, void *userptr)
{
	int serialNo;
	CPhidget_getSerialNumber(ENC, &serialNo);
	printf("Encoder %10d detached! \n", serialNo);

	return 0;
}

int CCONV errorHandler(CPhidgetHandle ENC, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s \n", ErrorCode, Description);

	return 0;
}

int CCONV inputChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int State)
{
	printf("Input #%i - State: %i \n", Index, State);

	return 0;
}

int CCONV positionChangeHandler(CPhidgetEncoderHandle ENC, void *usrptr, int Index, int Time, int RelativePosition)
{
	int Position;
	CPhidgetEncoder_getPosition(ENC, Index, &Position);
	global_pose = Position;
	uipmod = uip*conv;
	error = uipmod - global_pose;
	PID();
	return 0;
}

int display_properties(CPhidgetEncoderHandle phid)
{
	int serialNo, version, num_inputs, num_encoders;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetEncoder_getInputCount(phid, &num_inputs);
	CPhidgetEncoder_getEncoderCount(phid, &num_encoders);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("Num Encoders: %d\nNum Inputs: %d\n", num_encoders, num_inputs);

	return 0;
}

int encoder_simple()
{
	int result;

	const char *err;

	//Declare an encoder handle
	CPhidgetEncoderHandle encoder = 0;

	//create the encoder object
	CPhidgetEncoder_create(&encoder);

	//Set the handlers to be run when the device is plugged in or opened from software, unplugged or closed from software, or generates an error.
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)encoder, attachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)encoder, detachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)encoder, errorHandler, NULL);

	//Registers a callback that will run if an input changes.
	//Requires the handle for the Phidget, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetEncoder_set_OnInputChange_Handler(encoder, inputChangeHandler, NULL);

	//Registers a callback that will run if the encoder changes.
	//Requires the handle for the Encoder, the function that will be called, and an arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetEncoder_set_OnPositionChange_Handler(encoder, positionChangeHandler, NULL);


	CPhidget_open((CPhidgetHandle)encoder, 343099);

	//get the program to wait for an encoder device to be attached
	printf("Waiting for encoder to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)encoder, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}


	//Display the properties of the attached encoder device
	display_properties(encoder);

	return 0;
}

int motorcontrol_simple()
{
	int result;
	const char *err;
	int lastval;
	//Declare a motor control handle
	CPhidgetMotorControlHandle motoControl = 0;
	CPhidgetEncoderHandle encoder = 0;

	//create the motor control object
	CPhidgetMotorControl_create(&motoControl);
	CPhidgetEncoder_create(&encoder);

	//Set the handlers to be run when the device is plugged in or opened from software, unplugged or closed from software, or generates an error.
	CPhidget_set_OnAttach_Handler((CPhidgetHandle)motoControl, AttachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)motoControl, DetachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)motoControl, ErrorHandler, NULL);

	//Registers a callback that will run if an input changes.
	//Requires the handle for the Phidget, the function that will be called, and a arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetMotorControl_set_OnInputChange_Handler(motoControl, InputChangeHandler, NULL);

	//Registers a callback that will run if a motor changes.
	//Requires the handle for the Phidget, the function that will be called, and a arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetMotorControl_set_OnVelocityChange_Handler(motoControl, VelocityChangeHandler, NULL);

	//Registers a callback that will run if the current draw changes.
	//Requires the handle for the Phidget, the function that will be called, and a arbitrary pointer that will be supplied to the callback function (may be NULL).
	CPhidgetMotorControl_set_OnCurrentChange_Handler(motoControl, CurrentChangeHandler, NULL);

	/*CPhidgetMotorControl_set_OnEncoderPositionChange_Handler(motoControl, EncoderPositionChangeHandler, NULL);*/
	//open the motor control for device connections
	CPhidget_open((CPhidgetHandle)motoControl, 393011);

	//get the program to wait for a motor control device to be attached

	//=============================================================================================================

	printf("Waiting for MotorControl to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)motoControl, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	//Display the properties of the attached motor control device
	display_properties(motoControl);

	printf("Please input Angle of Rotation:\n");
	cin >> uip_initial;

	t = clock();
	do
	{
		if (abs(uip_initial) > 360)
		{
			printf("The value exceeds the physical limits of the system\n");
			getchar();

		}
		else
		{
			uip = uip_initial / 360 * 2 * pi*9.525;

			cout << "=========================================" << endl;
			cout << "Building up error to get the motor moving" << endl;
			cout << "=========================================" << endl;
			CPhidgetMotorControl_setAcceleration(motoControl, 1, 100.00);
			CPhidgetMotorControl_setVelocity(motoControl, 1, uip / abs(uip) * 70);
			Sleep(30);	// the error is built up during this interval of time

			cout << "Current scaled error: " << error / 1227.122 << endl;
			cout << "Current velocity: " << velocity << endl;
			cout << "Current position: " << global_pose << endl;
			cout << "==========================================" << endl;
			cout << "Error built up" << endl << endl << endl;
			int count = 0;
			double prevError = error;
			do
			{
				++count;

				CPhidgetMotorControl_setAcceleration(motoControl, 1, 50.00);
				CPhidgetMotorControl_setVelocity(motoControl, 1, dutycycle);

				if (count % 500 == 0)
				{
					cout << ">>>>>>>>>> Error:" << error / 1227.122 << "<<<<<<<<<<" << endl;
				}
				if (error*prevError < 0) {
					cout << "=================================================================" << endl;
					cout << "Direction Changed" << endl;
					cout << "Current Velocity: " << velocity << endl;
					cout << "Error ratio (abs(error) / deadband): " << abs(error / 1227.122) / deadband << endl;
					cout << "=================================================================" << endl;

				}
				prevError = error;

				if (abs(error) > 180 * conv)
				{
					cout << "=================================================================" << endl;
					cout << "The motor has gone cray... We need to exit... " << endl;
					cout << "=================================================================" << endl;
					break;
				}
			} while (abs(error) / conv >= deadband);
			/*cout << "The Closing Velocity is:" << dutycycle << endl;
			cout << "The Closing error is:" << error / conv << endl;
			cout << "The Encoder Position is:" << global_pose << endl;*/
			lastval = global_pose;
		}

		angle_error = (error / conv * 360) / (2 * pi*9.525);
		CPhidgetMotorControl_setAcceleration(motoControl, 1, 0);
		CPhidgetMotorControl_setVelocity(motoControl, 1, 0);
		cout << "The Closing Angular Error is:" << angle_error << endl;
		cout << "The Encoder Position is:" << global_pose << endl;


		// ************************Retraction process begins from this part *******************************************

		uip = 0;
		


		cout << "=========================================" << endl;
		cout << "Building up error to get the motor moving" << endl;
		cout << "=========================================" << endl;
		CPhidgetMotorControl_setAcceleration(motoControl, 1, 100.00);
		CPhidgetMotorControl_setVelocity(motoControl, 1, uip / abs(uip) * 70);
		
		Sleep(30);

		cout << "Current scaled error: " << error / conv << endl;
		cout << "Current velocity: " << velocity << endl;
		cout << "Current position: " << global_pose << endl;
		cout << "==========================================" << endl;
		cout << "Error built up" << endl << endl << endl;
		int count = 0;
		double prevError = error;
		do
		{
			++count;

			CPhidgetMotorControl_setAcceleration(motoControl, 1, 50.00);
			CPhidgetMotorControl_setVelocity(motoControl, 1, dutycycle);

			if (count % 500 == 0)
			{
				cout << ">>>>>>>>>> Error:" << error / conv << "<<<<<<<<<<" << endl;
			}
			if (error*prevError < 0) {
				cout << "=================================================================" << endl;
				cout << "Direction Changed" << endl;
				cout << "Current Velocity: " << velocity << endl;
				cout << "Error ratio (abs(error) / deadband): " << abs(error / conv) / deadband << endl;
				cout << "=================================================================" << endl;

			}
			prevError = error;


		} while (abs(error) / 1227.122 >= deadband);


		angle_error = (error / conv * 360) / (2 * pi*9.525);

		CPhidgetMotorControl_setAcceleration(motoControl, 1, 0);
		CPhidgetMotorControl_setVelocity(motoControl, 1, 0);
		
		cout << "The Closing Velocity is:" << dutycycle << endl;
		cout << "The Closing error is:" << angle_error << endl;
		cout << "The Encoder Position is:" << global_pose << endl;
		uip = uip_initial;
		counter++;

	} while (counter != 5);
	t = clock() - t;
	printf("It took me %d clicks (%f seconds).\n", t, ((float)t) / CLOCKS_PER_SEC);
	CPhidgetMotorControl_setAcceleration(motoControl, 1, 0);
	CPhidgetMotorControl_setVelocity(motoControl, 1, 0);
	time_val = ((float)t) / CLOCKS_PER_SEC;
	char dummy;
	cout << "Enter a character and press enter to exit..." << endl;
	cin >> dummy;

		//since user input has been read, this is a signal to terminate the program so we will close the phidget and delete the object we created
		printf("Closing...\n");
		CPhidget_close((CPhidgetHandle)motoControl);
		CPhidget_delete((CPhidgetHandle)motoControl);
		CPhidget_close((CPhidgetHandle)encoder);
		CPhidget_delete((CPhidgetHandle)encoder);

		//all done, exit
		return 0;
	}
	

	



int main(int argc, char* argv[])
{
	myfile.open("Dynamic_Test.txt",ios::in|ios::out|ios::app );

	encoder_simple();
	motorcontrol_simple();
	myfile << "====================================================" << endl;
	myfile << "DYNAMIC TESTING DATA"<<endl;
	myfile << "Number of Oscillations:\t" << counter <<endl;
	myfile << "Angle of Actuation:\t\t"<<  uip_initial<<endl;
	myfile << "Time Duration(s):\t\t" << time_val<<endl;
	myfile << "Frequency (Hz):\t\t\t" <<  counter / time_val<<endl;
	myfile << "====================================================" << endl;
	
	
	return 0;
}