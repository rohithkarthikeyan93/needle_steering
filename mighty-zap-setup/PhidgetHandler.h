#include "stdafx.h"
#include "phidget22.h"

namespace PhidgetHandler
{
	class phidgetHandler
	{
	public:

		PhidgetStepperHandle ch;
		PhidgetReturnCode res;
		const char *errs;
		static void CCONV onAttachHandler(PhidgetHandle phid, void *ctx);
		static void CCONV onDetachHandler(PhidgetHandle phid, void *ctx);
		static void CCONV errorHandler(PhidgetHandle phid, void *ctx, Phidget_ErrorEventCode errorCode, const char *errorString);
		static void CCONV onPositionChangeHandler(PhidgetStepperHandle ch, void *ctx, double position);
		static void CCONV onVelocityChangeHandler(PhidgetStepperHandle ch, void *ctx, double velocity);
		static void CCONV onStoppedHandler(PhidgetStepperHandle ch, void *ctx);
		static PhidgetReturnCode CCONV initChannel(PhidgetHandle ch);
		//void PhidgetStepper_setTargetPosition(PhidgetHandle ch, double novint_calibrated_x);
		phidgetHandler initializeStepper();

	};
}





