#pragma once
#include "stdafx.h"
#include "PhidgetHandler.h"
#include "phidget22.h"
#include "NovintHandler.h"
#include "MightyZapHandler.h"
#include "VisionHandler.h"
#include "ImageFunction.h"
#include "OpenGlRender.h"
#include "PathGenerator.h"
#include <iostream>
#include <sstream>
#include <atlstr.h>
#include <stdlib.h>
#include <fstream>

namespace NeedleTrackingProject {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace NovintHandler;
	using namespace PhidgetHandler;
	using namespace MightyZapHandler;
	using namespace VisionHandler;
	using namespace ImageFunction;
	using namespace OpenGLRender;
	using namespace PathGenerator;
	using namespace System::Threading;

	// object declaration of various classes //
	novintHandler novintHandlerobj;
	phidgetHandler phidgetHandlerobj;
	mightyZapHandler mightyZapHandlerobj;
	visionHandler visionHandlerobj1;
	visionHandler visionHandlerobj2;
	mat2picture mat2bmp;
	pathGenerator pathGeneratorobj;

	// Variables for trackbars of HSV calibration //
	int H_MIN1 = 0;
	int H_MAX1 = 256;
	int S_MIN1 = 0;
	int S_MAX1 = 256;
	int V_MIN1 = 0;
	int V_MAX1 = 256;

	const string trackbarWindowName1 = "Trackbars";

	void on_trackbar(int, void*)  
	{//This function gets called whenever a
	 // trackbar position is changed

	}

	void createTrackbars() {
		namedWindow(trackbarWindowName1, 1);
		//create memory to store trackbar name on window
		char TrackbarName[50];

		sprintf(TrackbarName, "H_MIN", H_MIN1);
		sprintf(TrackbarName, "H_MAX", H_MAX1);
		sprintf(TrackbarName, "S_MIN", S_MIN1);
		sprintf(TrackbarName, "S_MAX", S_MAX1);
		sprintf(TrackbarName, "V_MIN", V_MIN1);
		sprintf(TrackbarName, "V_MAX", V_MAX1);

		createTrackbar("H_MIN", trackbarWindowName1, &H_MIN1, H_MAX1, on_trackbar);
		createTrackbar("H_MAX", trackbarWindowName1, &H_MAX1, H_MAX1, on_trackbar);
		createTrackbar("S_MIN", trackbarWindowName1, &S_MIN1, S_MAX1, on_trackbar);
		createTrackbar("S_MAX", trackbarWindowName1, &S_MAX1, S_MAX1, on_trackbar);
		createTrackbar("V_MIN", trackbarWindowName1, &V_MIN1, V_MAX1, on_trackbar);
		createTrackbar("V_MAX", trackbarWindowName1, &V_MAX1, V_MAX1, on_trackbar);

	}



	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MainForm : public System::Windows::Forms::Form
	{

		double novint_x;
		double novint_y;
		double novint_z;

		double sensitivityFactor;  // variable for varying sensitivity of novint's motion to actuate the motors
		double sensitivityFraction = 1;

		int axis_z_max_travel = 150; //// max stroke in terms of mightyzap stroke units
		int axis_y_max_travel = 150; // max stroke in terms of mightyzap stroke units
		int axis_x_max_travel = 90000; // max no. of steps for stepper motor

		int max_motor_position = 100; 
		int min_motor_position = -100;

		double novint_calibrated_x;
		double novint_calibrated_y;
		double novint_calibrated_z;
		double derivative_x, derivative_y, derivative_z;
		
		int phidgetOpened = 1; // variable to initiate stepper motor
		int pictureCount = 0; // variable to show or hide the web cam feed
		int OpenGLcount = 0; // variable to create OpenGL window
		int cameraViewCount = 0; // variable to change cameraview in graphic window

		bool savedRefPoints = false; 

	private: System::Windows::Forms::TabControl^  tabControl;
	private: System::Windows::Forms::TabPage^  HomeTab;
	private: System::Windows::Forms::TabPage^  VisionTab;

	private: System::Windows::Forms::PictureBox^  WebCamFeed2;
	private: System::Windows::Forms::PictureBox^  WebCamFeed1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::CheckBox^  checkBox1;
	private: System::Windows::Forms::CheckBox^  Camera_On_Off_switch;
	private: System::Windows::Forms::Button^  DisEngagingButton;
	private: System::Windows::Forms::Button^  EngageMotor;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorkerForNovintInteraction;
	private: System::Windows::Forms::Label^  MotorInfoLabel;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorkerForCamera;
	private: System::Windows::Forms::Timer^  timerForOpenGLRender;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  DisEngagingbutton_HomeTab;
	private: System::Windows::Forms::Button^  EngageMotor_HomeTab;
	private: System::Windows::Forms::TextBox^  target_z;
	private: System::Windows::Forms::TextBox^  target_y;
	private: System::Windows::Forms::TextBox^  target_x;
	private: System::Windows::Forms::TextBox^  start_z;
	private: System::Windows::Forms::TextBox^  start_y;
	private: System::Windows::Forms::TextBox^  start_x;
	private: System::Windows::Forms::TextBox^  obstacle3_r;
	private: System::Windows::Forms::TextBox^  obstacle3_z;
	private: System::Windows::Forms::TextBox^  obstacle3_y;
	private: System::Windows::Forms::TextBox^  obstacle3_x;
	private: System::Windows::Forms::TextBox^  obstacle2_r;
	private: System::Windows::Forms::TextBox^  obstacle2_z;
	private: System::Windows::Forms::TextBox^  obstacle2_y;
	private: System::Windows::Forms::TextBox^  obstacle2_x;
	private: System::Windows::Forms::TextBox^  obstacle1_r;
	private: System::Windows::Forms::TextBox^  obstacle1_z;
	private: System::Windows::Forms::TextBox^  obstacle1_y;
	private: System::Windows::Forms::TextBox^  obstacle1_x;
	private: System::Windows::Forms::Button^  removetarget;
	private: System::Windows::Forms::Button^  removestart;
	private: System::Windows::Forms::Button^  settarget;
	private: System::Windows::Forms::Button^  setstart;
	private: System::Windows::Forms::Button^  removeobstacle_3;
	private: System::Windows::Forms::Button^  setobstacle_3;
	private: System::Windows::Forms::Button^  removeobstacle_2;
	private: System::Windows::Forms::Button^  setobstacle_2;
	private: System::Windows::Forms::Button^  removeobstacle_1;
	private: System::Windows::Forms::Button^  setobstacle_1;
	private: System::Windows::Forms::Button^  generatepath;
	private: System::ComponentModel::BackgroundWorker^  backgroundWorkerForPathGeneration;

			 


	public:
		MainForm(void)
		{
			InitializeComponent();

			tabControl->SelectedIndex = 0;

			// assigning some default values to the textboxes//

			this->obstacle1_x->Text = "30";
			this->obstacle1_y->Text = "58";
			this->obstacle1_z->Text = "20";
			this->obstacle1_r->Text = "10";

			this->obstacle2_x->Text = "70";
			this->obstacle2_y->Text = "60";
			this->obstacle2_z->Text = "20";
			this->obstacle2_r->Text = "7";

			this->obstacle3_x->Text = "90";
			this->obstacle3_y->Text = "38";
			this->obstacle3_z->Text = "20";
			this->obstacle3_r->Text = "4";

			this->start_x->Text = "0";
			this->start_y->Text = "80";
			this->start_z->Text = "20";

			this->target_x->Text = "110";
			this->target_y->Text = "30";
			this->target_z->Text = "20";

			// initiatiating the mighty zap motors and novint falcon//

			mightyZapHandlerobj.initializeMightyZapMotors(mightyZapHandlerobj);

			novintHandlerobj = novintHandlerobj.initializeHapticThread();

			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MainForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::ComponentModel::IContainer^  components;
	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>

		OpenGLRender::COpenGL ^ OpenGL;


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->tabControl = (gcnew System::Windows::Forms::TabControl());
			this->HomeTab = (gcnew System::Windows::Forms::TabPage());
			this->generatepath = (gcnew System::Windows::Forms::Button());
			this->removetarget = (gcnew System::Windows::Forms::Button());
			this->removestart = (gcnew System::Windows::Forms::Button());
			this->settarget = (gcnew System::Windows::Forms::Button());
			this->setstart = (gcnew System::Windows::Forms::Button());
			this->removeobstacle_3 = (gcnew System::Windows::Forms::Button());
			this->setobstacle_3 = (gcnew System::Windows::Forms::Button());
			this->removeobstacle_2 = (gcnew System::Windows::Forms::Button());
			this->setobstacle_2 = (gcnew System::Windows::Forms::Button());
			this->removeobstacle_1 = (gcnew System::Windows::Forms::Button());
			this->setobstacle_1 = (gcnew System::Windows::Forms::Button());
			this->target_z = (gcnew System::Windows::Forms::TextBox());
			this->target_y = (gcnew System::Windows::Forms::TextBox());
			this->target_x = (gcnew System::Windows::Forms::TextBox());
			this->start_z = (gcnew System::Windows::Forms::TextBox());
			this->start_y = (gcnew System::Windows::Forms::TextBox());
			this->start_x = (gcnew System::Windows::Forms::TextBox());
			this->obstacle3_r = (gcnew System::Windows::Forms::TextBox());
			this->obstacle3_z = (gcnew System::Windows::Forms::TextBox());
			this->obstacle3_y = (gcnew System::Windows::Forms::TextBox());
			this->obstacle3_x = (gcnew System::Windows::Forms::TextBox());
			this->obstacle2_r = (gcnew System::Windows::Forms::TextBox());
			this->obstacle2_z = (gcnew System::Windows::Forms::TextBox());
			this->obstacle2_y = (gcnew System::Windows::Forms::TextBox());
			this->obstacle2_x = (gcnew System::Windows::Forms::TextBox());
			this->obstacle1_r = (gcnew System::Windows::Forms::TextBox());
			this->obstacle1_z = (gcnew System::Windows::Forms::TextBox());
			this->obstacle1_y = (gcnew System::Windows::Forms::TextBox());
			this->obstacle1_x = (gcnew System::Windows::Forms::TextBox());
			this->DisEngagingbutton_HomeTab = (gcnew System::Windows::Forms::Button());
			this->EngageMotor_HomeTab = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->VisionTab = (gcnew System::Windows::Forms::TabPage());
			this->MotorInfoLabel = (gcnew System::Windows::Forms::Label());
			this->WebCamFeed2 = (gcnew System::Windows::Forms::PictureBox());
			this->WebCamFeed1 = (gcnew System::Windows::Forms::PictureBox());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
			this->Camera_On_Off_switch = (gcnew System::Windows::Forms::CheckBox());
			this->DisEngagingButton = (gcnew System::Windows::Forms::Button());
			this->EngageMotor = (gcnew System::Windows::Forms::Button());
			this->backgroundWorkerForNovintInteraction = (gcnew System::ComponentModel::BackgroundWorker());
			this->backgroundWorkerForCamera = (gcnew System::ComponentModel::BackgroundWorker());
			this->timerForOpenGLRender = (gcnew System::Windows::Forms::Timer(this->components));
			this->backgroundWorkerForPathGeneration = (gcnew System::ComponentModel::BackgroundWorker());
			this->tabControl->SuspendLayout();
			this->HomeTab->SuspendLayout();
			this->VisionTab->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->WebCamFeed2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->WebCamFeed1))->BeginInit();
			this->SuspendLayout();
			// 
			// tabControl
			// 
			this->tabControl->Controls->Add(this->HomeTab);
			this->tabControl->Controls->Add(this->VisionTab);
			this->tabControl->Location = System::Drawing::Point(-2, 12);
			this->tabControl->Name = L"tabControl";
			this->tabControl->SelectedIndex = 0;
			this->tabControl->Size = System::Drawing::Size(1369, 864);
			this->tabControl->TabIndex = 0;
			// 
			// HomeTab
			// 
			this->HomeTab->Controls->Add(this->generatepath);
			this->HomeTab->Controls->Add(this->removetarget);
			this->HomeTab->Controls->Add(this->removestart);
			this->HomeTab->Controls->Add(this->settarget);
			this->HomeTab->Controls->Add(this->setstart);
			this->HomeTab->Controls->Add(this->removeobstacle_3);
			this->HomeTab->Controls->Add(this->setobstacle_3);
			this->HomeTab->Controls->Add(this->removeobstacle_2);
			this->HomeTab->Controls->Add(this->setobstacle_2);
			this->HomeTab->Controls->Add(this->removeobstacle_1);
			this->HomeTab->Controls->Add(this->setobstacle_1);
			this->HomeTab->Controls->Add(this->target_z);
			this->HomeTab->Controls->Add(this->target_y);
			this->HomeTab->Controls->Add(this->target_x);
			this->HomeTab->Controls->Add(this->start_z);
			this->HomeTab->Controls->Add(this->start_y);
			this->HomeTab->Controls->Add(this->start_x);
			this->HomeTab->Controls->Add(this->obstacle3_r);
			this->HomeTab->Controls->Add(this->obstacle3_z);
			this->HomeTab->Controls->Add(this->obstacle3_y);
			this->HomeTab->Controls->Add(this->obstacle3_x);
			this->HomeTab->Controls->Add(this->obstacle2_r);
			this->HomeTab->Controls->Add(this->obstacle2_z);
			this->HomeTab->Controls->Add(this->obstacle2_y);
			this->HomeTab->Controls->Add(this->obstacle2_x);
			this->HomeTab->Controls->Add(this->obstacle1_r);
			this->HomeTab->Controls->Add(this->obstacle1_z);
			this->HomeTab->Controls->Add(this->obstacle1_y);
			this->HomeTab->Controls->Add(this->obstacle1_x);
			this->HomeTab->Controls->Add(this->DisEngagingbutton_HomeTab);
			this->HomeTab->Controls->Add(this->EngageMotor_HomeTab);
			this->HomeTab->Controls->Add(this->label1);
			this->HomeTab->Controls->Add(this->button2);
			this->HomeTab->Location = System::Drawing::Point(4, 25);
			this->HomeTab->Name = L"HomeTab";
			this->HomeTab->Padding = System::Windows::Forms::Padding(3);
			this->HomeTab->Size = System::Drawing::Size(1361, 835);
			this->HomeTab->TabIndex = 0;
			this->HomeTab->Text = L"Home";
			this->HomeTab->UseVisualStyleBackColor = true;
			this->HomeTab->Enter += gcnew System::EventHandler(this, &MainForm::HomeTab_Enter);
			// 
			// generatepath
			// 
			this->generatepath->Location = System::Drawing::Point(20, 709);
			this->generatepath->Name = L"generatepath";
			this->generatepath->Size = System::Drawing::Size(123, 44);
			this->generatepath->TabIndex = 33;
			this->generatepath->Text = L"Generate Path";
			this->generatepath->UseVisualStyleBackColor = true;
			this->generatepath->Click += gcnew System::EventHandler(this, &MainForm::generatepath_Click);
			// 
			// removetarget
			// 
			this->removetarget->Location = System::Drawing::Point(159, 594);
			this->removetarget->Name = L"removetarget";
			this->removetarget->Size = System::Drawing::Size(123, 44);
			this->removetarget->TabIndex = 32;
			this->removetarget->Text = L"Remove Target";
			this->removetarget->UseVisualStyleBackColor = true;
			this->removetarget->Click += gcnew System::EventHandler(this, &MainForm::removetarget_Click);
			// 
			// removestart
			// 
			this->removestart->Location = System::Drawing::Point(159, 481);
			this->removestart->Name = L"removestart";
			this->removestart->Size = System::Drawing::Size(123, 44);
			this->removestart->TabIndex = 31;
			this->removestart->Text = L"Remove Start";
			this->removestart->UseVisualStyleBackColor = true;
			this->removestart->Click += gcnew System::EventHandler(this, &MainForm::removestart_Click);
			// 
			// settarget
			// 
			this->settarget->Location = System::Drawing::Point(20, 594);
			this->settarget->Name = L"settarget";
			this->settarget->Size = System::Drawing::Size(123, 44);
			this->settarget->TabIndex = 30;
			this->settarget->Text = L"Set Target";
			this->settarget->UseVisualStyleBackColor = true;
			this->settarget->Click += gcnew System::EventHandler(this, &MainForm::settarget_Click);
			// 
			// setstart
			// 
			this->setstart->Location = System::Drawing::Point(20, 481);
			this->setstart->Name = L"setstart";
			this->setstart->Size = System::Drawing::Size(123, 44);
			this->setstart->TabIndex = 29;
			this->setstart->Text = L"Set Start";
			this->setstart->UseVisualStyleBackColor = true;
			this->setstart->Click += gcnew System::EventHandler(this, &MainForm::setstart_Click);
			// 
			// removeobstacle_3
			// 
			this->removeobstacle_3->Location = System::Drawing::Point(159, 377);
			this->removeobstacle_3->Name = L"removeobstacle_3";
			this->removeobstacle_3->Size = System::Drawing::Size(123, 44);
			this->removeobstacle_3->TabIndex = 28;
			this->removeobstacle_3->Text = L"Remove Obstacle";
			this->removeobstacle_3->UseVisualStyleBackColor = true;
			this->removeobstacle_3->Click += gcnew System::EventHandler(this, &MainForm::removeobstacle_3_Click);
			// 
			// setobstacle_3
			// 
			this->setobstacle_3->Location = System::Drawing::Point(20, 377);
			this->setobstacle_3->Name = L"setobstacle_3";
			this->setobstacle_3->Size = System::Drawing::Size(123, 44);
			this->setobstacle_3->TabIndex = 27;
			this->setobstacle_3->Text = L"Set Obstacle";
			this->setobstacle_3->UseVisualStyleBackColor = true;
			this->setobstacle_3->Click += gcnew System::EventHandler(this, &MainForm::setobstacle_3_Click);
			// 
			// removeobstacle_2
			// 
			this->removeobstacle_2->Location = System::Drawing::Point(159, 277);
			this->removeobstacle_2->Name = L"removeobstacle_2";
			this->removeobstacle_2->Size = System::Drawing::Size(123, 44);
			this->removeobstacle_2->TabIndex = 26;
			this->removeobstacle_2->Text = L"Remove Obstacle";
			this->removeobstacle_2->UseVisualStyleBackColor = true;
			this->removeobstacle_2->Click += gcnew System::EventHandler(this, &MainForm::removeobstacle_2_Click);
			// 
			// setobstacle_2
			// 
			this->setobstacle_2->Location = System::Drawing::Point(20, 277);
			this->setobstacle_2->Name = L"setobstacle_2";
			this->setobstacle_2->Size = System::Drawing::Size(123, 44);
			this->setobstacle_2->TabIndex = 25;
			this->setobstacle_2->Text = L"Set Obstacle";
			this->setobstacle_2->UseVisualStyleBackColor = true;
			this->setobstacle_2->Click += gcnew System::EventHandler(this, &MainForm::setobstacle_2_Click);
			// 
			// removeobstacle_1
			// 
			this->removeobstacle_1->Location = System::Drawing::Point(159, 192);
			this->removeobstacle_1->Name = L"removeobstacle_1";
			this->removeobstacle_1->Size = System::Drawing::Size(123, 44);
			this->removeobstacle_1->TabIndex = 24;
			this->removeobstacle_1->Text = L"Remove Obstacle";
			this->removeobstacle_1->UseVisualStyleBackColor = true;
			this->removeobstacle_1->Click += gcnew System::EventHandler(this, &MainForm::removeobstacle_1_Click);
			// 
			// setobstacle_1
			// 
			this->setobstacle_1->Location = System::Drawing::Point(20, 192);
			this->setobstacle_1->Name = L"setobstacle_1";
			this->setobstacle_1->Size = System::Drawing::Size(123, 44);
			this->setobstacle_1->TabIndex = 23;
			this->setobstacle_1->Text = L"Set Obstacle";
			this->setobstacle_1->UseVisualStyleBackColor = true;
			this->setobstacle_1->Click += gcnew System::EventHandler(this, &MainForm::setobstacle_1_Click);
			// 
			// target_z
			// 
			this->target_z->Location = System::Drawing::Point(180, 540);
			this->target_z->Multiline = true;
			this->target_z->Name = L"target_z";
			this->target_z->Size = System::Drawing::Size(52, 29);
			this->target_z->TabIndex = 22;
			// 
			// target_y
			// 
			this->target_y->Location = System::Drawing::Point(101, 540);
			this->target_y->Multiline = true;
			this->target_y->Name = L"target_y";
			this->target_y->Size = System::Drawing::Size(52, 29);
			this->target_y->TabIndex = 21;
			// 
			// target_x
			// 
			this->target_x->Location = System::Drawing::Point(20, 540);
			this->target_x->Multiline = true;
			this->target_x->Name = L"target_x";
			this->target_x->Size = System::Drawing::Size(52, 29);
			this->target_x->TabIndex = 20;
			// 
			// start_z
			// 
			this->start_z->Location = System::Drawing::Point(180, 436);
			this->start_z->Multiline = true;
			this->start_z->Name = L"start_z";
			this->start_z->Size = System::Drawing::Size(52, 29);
			this->start_z->TabIndex = 19;
			// 
			// start_y
			// 
			this->start_y->Location = System::Drawing::Point(101, 436);
			this->start_y->Multiline = true;
			this->start_y->Name = L"start_y";
			this->start_y->Size = System::Drawing::Size(52, 29);
			this->start_y->TabIndex = 18;
			// 
			// start_x
			// 
			this->start_x->Location = System::Drawing::Point(20, 436);
			this->start_x->Multiline = true;
			this->start_x->Name = L"start_x";
			this->start_x->Size = System::Drawing::Size(52, 29);
			this->start_x->TabIndex = 17;
			// 
			// obstacle3_r
			// 
			this->obstacle3_r->Location = System::Drawing::Point(247, 341);
			this->obstacle3_r->Multiline = true;
			this->obstacle3_r->Name = L"obstacle3_r";
			this->obstacle3_r->Size = System::Drawing::Size(52, 29);
			this->obstacle3_r->TabIndex = 16;
			// 
			// obstacle3_z
			// 
			this->obstacle3_z->Location = System::Drawing::Point(180, 341);
			this->obstacle3_z->Multiline = true;
			this->obstacle3_z->Name = L"obstacle3_z";
			this->obstacle3_z->Size = System::Drawing::Size(52, 29);
			this->obstacle3_z->TabIndex = 15;
			// 
			// obstacle3_y
			// 
			this->obstacle3_y->Location = System::Drawing::Point(101, 341);
			this->obstacle3_y->Multiline = true;
			this->obstacle3_y->Name = L"obstacle3_y";
			this->obstacle3_y->Size = System::Drawing::Size(52, 29);
			this->obstacle3_y->TabIndex = 14;
			// 
			// obstacle3_x
			// 
			this->obstacle3_x->Location = System::Drawing::Point(20, 341);
			this->obstacle3_x->Multiline = true;
			this->obstacle3_x->Name = L"obstacle3_x";
			this->obstacle3_x->Size = System::Drawing::Size(52, 29);
			this->obstacle3_x->TabIndex = 13;
			// 
			// obstacle2_r
			// 
			this->obstacle2_r->Location = System::Drawing::Point(247, 242);
			this->obstacle2_r->Multiline = true;
			this->obstacle2_r->Name = L"obstacle2_r";
			this->obstacle2_r->Size = System::Drawing::Size(52, 29);
			this->obstacle2_r->TabIndex = 12;
			// 
			// obstacle2_z
			// 
			this->obstacle2_z->Location = System::Drawing::Point(180, 242);
			this->obstacle2_z->Multiline = true;
			this->obstacle2_z->Name = L"obstacle2_z";
			this->obstacle2_z->Size = System::Drawing::Size(52, 29);
			this->obstacle2_z->TabIndex = 11;
			// 
			// obstacle2_y
			// 
			this->obstacle2_y->Location = System::Drawing::Point(101, 242);
			this->obstacle2_y->Multiline = true;
			this->obstacle2_y->Name = L"obstacle2_y";
			this->obstacle2_y->Size = System::Drawing::Size(52, 29);
			this->obstacle2_y->TabIndex = 10;
			// 
			// obstacle2_x
			// 
			this->obstacle2_x->Location = System::Drawing::Point(20, 242);
			this->obstacle2_x->Multiline = true;
			this->obstacle2_x->Name = L"obstacle2_x";
			this->obstacle2_x->Size = System::Drawing::Size(52, 29);
			this->obstacle2_x->TabIndex = 9;
			// 
			// obstacle1_r
			// 
			this->obstacle1_r->Location = System::Drawing::Point(247, 157);
			this->obstacle1_r->Multiline = true;
			this->obstacle1_r->Name = L"obstacle1_r";
			this->obstacle1_r->Size = System::Drawing::Size(52, 29);
			this->obstacle1_r->TabIndex = 8;
			// 
			// obstacle1_z
			// 
			this->obstacle1_z->Location = System::Drawing::Point(180, 157);
			this->obstacle1_z->Multiline = true;
			this->obstacle1_z->Name = L"obstacle1_z";
			this->obstacle1_z->Size = System::Drawing::Size(52, 29);
			this->obstacle1_z->TabIndex = 7;
			// 
			// obstacle1_y
			// 
			this->obstacle1_y->Location = System::Drawing::Point(101, 157);
			this->obstacle1_y->Multiline = true;
			this->obstacle1_y->Name = L"obstacle1_y";
			this->obstacle1_y->Size = System::Drawing::Size(52, 29);
			this->obstacle1_y->TabIndex = 6;
			// 
			// obstacle1_x
			// 
			this->obstacle1_x->Location = System::Drawing::Point(20, 157);
			this->obstacle1_x->Multiline = true;
			this->obstacle1_x->Name = L"obstacle1_x";
			this->obstacle1_x->Size = System::Drawing::Size(52, 29);
			this->obstacle1_x->TabIndex = 5;
			// 
			// DisEngagingbutton_HomeTab
			// 
			this->DisEngagingbutton_HomeTab->Location = System::Drawing::Point(10, 65);
			this->DisEngagingbutton_HomeTab->Name = L"DisEngagingbutton_HomeTab";
			this->DisEngagingbutton_HomeTab->Size = System::Drawing::Size(154, 57);
			this->DisEngagingbutton_HomeTab->TabIndex = 4;
			this->DisEngagingbutton_HomeTab->Text = L"DIsengage Motor";
			this->DisEngagingbutton_HomeTab->UseVisualStyleBackColor = true;
			this->DisEngagingbutton_HomeTab->Click += gcnew System::EventHandler(this, &MainForm::DisEngagingbutton_HomeTab_Click);
			// 
			// EngageMotor_HomeTab
			// 
			this->EngageMotor_HomeTab->Location = System::Drawing::Point(10, 6);
			this->EngageMotor_HomeTab->Name = L"EngageMotor_HomeTab";
			this->EngageMotor_HomeTab->Size = System::Drawing::Size(154, 53);
			this->EngageMotor_HomeTab->TabIndex = 3;
			this->EngageMotor_HomeTab->Text = L"Engage Motor";
			this->EngageMotor_HomeTab->UseVisualStyleBackColor = true;
			this->EngageMotor_HomeTab->Click += gcnew System::EventHandler(this, &MainForm::EngageMotor_HomeTab_Click);
			// 
			// label1
			// 
			this->label1->Font = (gcnew System::Drawing::Font(L"Adobe Heiti Std R", 10.2F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(1164, 117);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(115, 36);
			this->label1->TabIndex = 1;
			this->label1->Text = L"Top View";
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(1136, 31);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(153, 54);
			this->button2->TabIndex = 0;
			this->button2->Text = L"Change View";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MainForm::button2_Click);
			// 
			// VisionTab
			// 
			this->VisionTab->Controls->Add(this->MotorInfoLabel);
			this->VisionTab->Controls->Add(this->WebCamFeed2);
			this->VisionTab->Controls->Add(this->WebCamFeed1);
			this->VisionTab->Controls->Add(this->button1);
			this->VisionTab->Controls->Add(this->checkBox1);
			this->VisionTab->Controls->Add(this->Camera_On_Off_switch);
			this->VisionTab->Controls->Add(this->DisEngagingButton);
			this->VisionTab->Controls->Add(this->EngageMotor);
			this->VisionTab->Location = System::Drawing::Point(4, 25);
			this->VisionTab->Name = L"VisionTab";
			this->VisionTab->Padding = System::Windows::Forms::Padding(3);
			this->VisionTab->Size = System::Drawing::Size(1361, 835);
			this->VisionTab->TabIndex = 1;
			this->VisionTab->Text = L"Vision Feedback";
			this->VisionTab->UseVisualStyleBackColor = true;
			this->VisionTab->Enter += gcnew System::EventHandler(this, &MainForm::VisionTab_Enter);
			// 
			// MotorInfoLabel
			// 
			this->MotorInfoLabel->Location = System::Drawing::Point(66, 528);
			this->MotorInfoLabel->Name = L"MotorInfoLabel";
			this->MotorInfoLabel->Size = System::Drawing::Size(100, 23);
			this->MotorInfoLabel->TabIndex = 7;
			this->MotorInfoLabel->Text = L"Motor Info";
			// 
			// WebCamFeed2
			// 
			this->WebCamFeed2->Location = System::Drawing::Point(324, 406);
			this->WebCamFeed2->Name = L"WebCamFeed2";
			this->WebCamFeed2->Size = System::Drawing::Size(644, 414);
			this->WebCamFeed2->TabIndex = 6;
			this->WebCamFeed2->TabStop = false;
			// 
			// WebCamFeed1
			// 
			this->WebCamFeed1->Location = System::Drawing::Point(324, 20);
			this->WebCamFeed1->Name = L"WebCamFeed1";
			this->WebCamFeed1->Size = System::Drawing::Size(644, 373);
			this->WebCamFeed1->TabIndex = 5;
			this->WebCamFeed1->TabStop = false;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(46, 406);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(159, 44);
			this->button1->TabIndex = 4;
			this->button1->Text = L"Save Reference Points";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainForm::button1_Click);
			// 
			// checkBox1
			// 
			this->checkBox1->Location = System::Drawing::Point(35, 332);
			this->checkBox1->Name = L"checkBox1";
			this->checkBox1->Size = System::Drawing::Size(170, 24);
			this->checkBox1->TabIndex = 3;
			this->checkBox1->Text = L"Calibration Mode";
			this->checkBox1->UseVisualStyleBackColor = true;
			this->checkBox1->CheckedChanged += gcnew System::EventHandler(this, &MainForm::checkBox1_CheckedChanged);
			// 
			// Camera_On_Off_switch
			// 
			this->Camera_On_Off_switch->Location = System::Drawing::Point(35, 283);
			this->Camera_On_Off_switch->Name = L"Camera_On_Off_switch";
			this->Camera_On_Off_switch->Size = System::Drawing::Size(170, 24);
			this->Camera_On_Off_switch->TabIndex = 2;
			this->Camera_On_Off_switch->Text = L"Camera On/Off";
			this->Camera_On_Off_switch->UseVisualStyleBackColor = true;
			this->Camera_On_Off_switch->CheckedChanged += gcnew System::EventHandler(this, &MainForm::Camera_On_Off_switch_CheckedChanged);
			// 
			// DisEngagingButton
			// 
			this->DisEngagingButton->Location = System::Drawing::Point(35, 175);
			this->DisEngagingButton->Name = L"DisEngagingButton";
			this->DisEngagingButton->Size = System::Drawing::Size(170, 54);
			this->DisEngagingButton->TabIndex = 1;
			this->DisEngagingButton->Text = L"Disengage Motors";
			this->DisEngagingButton->UseVisualStyleBackColor = true;
			this->DisEngagingButton->Click += gcnew System::EventHandler(this, &MainForm::DisEngagingButton_Click);
			// 
			// EngageMotor
			// 
			this->EngageMotor->Location = System::Drawing::Point(35, 77);
			this->EngageMotor->Name = L"EngageMotor";
			this->EngageMotor->Size = System::Drawing::Size(170, 54);
			this->EngageMotor->TabIndex = 0;
			this->EngageMotor->Text = L"Engage Motor";
			this->EngageMotor->UseVisualStyleBackColor = true;
			this->EngageMotor->Click += gcnew System::EventHandler(this, &MainForm::EngageMotor_Click);
			// 
			// backgroundWorkerForNovintInteraction
			// 
			this->backgroundWorkerForNovintInteraction->WorkerReportsProgress = true;
			this->backgroundWorkerForNovintInteraction->WorkerSupportsCancellation = true;
			this->backgroundWorkerForNovintInteraction->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MainForm::backgroundWorkerForNovintInteraction_DoWork);
			this->backgroundWorkerForNovintInteraction->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MainForm::backgroundWorkerForNovintInteraction_RunWorkerCompleted);
			// 
			// backgroundWorkerForCamera
			// 
			this->backgroundWorkerForCamera->WorkerReportsProgress = true;
			this->backgroundWorkerForCamera->WorkerSupportsCancellation = true;
			this->backgroundWorkerForCamera->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MainForm::backgroundWorkerForCamera_DoWork);
			// 
			// timerForOpenGLRender
			// 
			this->timerForOpenGLRender->Tick += gcnew System::EventHandler(this, &MainForm::timerForOpenGLRender_Tick);
			// 
			// backgroundWorkerForPathGeneration
			// 
			this->backgroundWorkerForPathGeneration->WorkerReportsProgress = true;
			this->backgroundWorkerForPathGeneration->DoWork += gcnew System::ComponentModel::DoWorkEventHandler(this, &MainForm::backgroundWorkerForPathGeneration_DoWork);
			this->backgroundWorkerForPathGeneration->RunWorkerCompleted += gcnew System::ComponentModel::RunWorkerCompletedEventHandler(this, &MainForm::backgroundWorkerForPathGeneration_RunWorkerCompleted);
			// 
			// MainForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(1379, 869);
			this->Controls->Add(this->tabControl);
			this->Name = L"MainForm";
			this->tabControl->ResumeLayout(false);
			this->HomeTab->ResumeLayout(false);
			this->HomeTab->PerformLayout();
			this->VisionTab->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->WebCamFeed2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->WebCamFeed1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void backgroundWorkerForNovintInteraction_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {

		BackgroundWorker^ NovintInteracter = dynamic_cast<BackgroundWorker^>(sender);

		bool novint_button0, novint_button1, novint_button2, novint_button3;

		novint_button0 = false;
		novint_button1 = false;
		novint_button2 = false;
		novint_button3 = false;

		cVector3d novintPosition;

		if (novintHandlerobj.hapticDevice != NULL)
			novintHandlerobj.hapticDevice->getPosition(novintPosition);

		novintHandlerobj.hapticDevice->getUserSwitch(0, novint_button0);
		novintHandlerobj.hapticDevice->getUserSwitch(2, novint_button2);

		novint_z = novintPosition.z();
		novint_y = novintPosition.y();
		novint_x = novintPosition.x();

		novint_calibrated_x = sensitivityFraction * (-(((axis_x_max_travel)*(novint_x - 0.035))) / 0.07);

		if (novint_calibrated_x > axis_x_max_travel)
			novint_calibrated_x = axis_x_max_travel;
		else if (novint_calibrated_x < 0)
			novint_calibrated_x = 0;
		else
			novint_calibrated_x = novint_calibrated_x;

		novint_calibrated_z = sensitivityFraction * (((2 * (axis_z_max_travel)*(novint_z + 0.05)) - (0.1*axis_z_max_travel)) / 0.1);

		if (novint_calibrated_z > max_motor_position)
			novint_calibrated_z = max_motor_position;
		else if (novint_calibrated_z < min_motor_position)
			novint_calibrated_z = min_motor_position;
		else
			novint_calibrated_z = novint_calibrated_z;

		novint_calibrated_y = sensitivityFraction * (((2 * (axis_y_max_travel)*(novint_y + 0.05)) - (0.1*axis_y_max_travel)) / 0.1);

		if (novint_calibrated_y > max_motor_position)
			novint_calibrated_y = max_motor_position;
		else if (novint_calibrated_y < min_motor_position)
			novint_calibrated_y = min_motor_position;
		else
			novint_calibrated_y = novint_calibrated_y;

		if (phidgetOpened == 1)
		{

			phidgetHandlerobj = phidgetHandlerobj.initializeStepper();
			phidgetOpened = 0;
		}

		if (NovintInteracter->CancellationPending)
		{
			Thread::Sleep(2000);

			e->Cancel = true;
		}

		if ((novint_button0 == true))
		{
			e->Result = "Stepper Engaged";
		}

		else
		{
			e->Result = "MightyZap Engaged";
		}
	}
private: System::Void backgroundWorkerForNovintInteraction_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {


	if (mightyZapHandlerobj.portOpenState == 0)
	{
		mightyZapHandlerobj.mightyzap->OpenMightyZap(mightyZapHandlerobj.p_name, CBR_57600);
		mightyZapHandlerobj.portOpenState = 1;
	}

	EngageMotor->Enabled = true;

	if (e->Error != nullptr)
	{
		MessageBox::Show(e->Error->Message);
	}
	else
		if (e->Cancelled)
		{

			MotorInfoLabel->Text = "Motor Disengaged!!";
		}
		else
		{

			if (e->Result == "MightyZap Engaged")
			{

				MotorInfoLabel->Text = "MightyZap Engaged";

				mightyZapHandlerobj.mapNovintToNeedleTip(novint_calibrated_z, novint_calibrated_y, mightyZapHandlerobj);

				EngageMotor->PerformClick();
				EngageMotor_HomeTab->PerformClick();
				

			}

			else if (e->Result == "Stepper Engaged")
			{
				mightyZapHandlerobj.mightyzap->CloseMightyZap();

				PhidgetStepper_setTargetPosition(phidgetHandlerobj.ch, novint_calibrated_x);

				mightyZapHandlerobj.portOpenState = 0;

				MotorInfoLabel->Text = "Stepper Engaged";

				EngageMotor->PerformClick();
				EngageMotor_HomeTab->PerformClick();

			}
		}
}
private: System::Void EngageMotor_Click(System::Object^  sender, System::EventArgs^  e) {

	this->DisEngagingButton->Enabled = true;

	backgroundWorkerForNovintInteraction->RunWorkerAsync(1);
}
private: System::Void DisEngagingButton_Click(System::Object^  sender, System::EventArgs^  e) {

	this->backgroundWorkerForNovintInteraction->CancelAsync();

	DisEngagingButton->Enabled = false;
}
private: System::Void Camera_On_Off_switch_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	if ((Camera_On_Off_switch->Checked == true) && (pictureCount == 0))
	{
		this->backgroundWorkerForCamera->RunWorkerAsync(1);
	}
	else if ((Camera_On_Off_switch->Checked == true) && (pictureCount == 1))
	{

		this->WebCamFeed1->Show();
		this->WebCamFeed2->Show();
	}
	else
	{
		this->WebCamFeed1->Hide();
		this->WebCamFeed1->Hide();
		pictureCount = 1;
	}
}
private: System::Void backgroundWorkerForCamera_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {


	BackgroundWorker^ CameraWorker = dynamic_cast<BackgroundWorker^>(sender);


	if (CameraWorker->CancellationPending)
	{
		Thread::Sleep(1200);

		e->Cancel = true;
	}

	visionHandlerobj1.capture.open(2);
	visionHandlerobj1.capture.set(CV_CAP_PROP_FRAME_WIDTH, 1271);
	visionHandlerobj1.capture.set(CV_CAP_PROP_FRAME_HEIGHT, 410);

	visionHandlerobj2.capture.open(1);
	visionHandlerobj2.capture.set(CV_CAP_PROP_FRAME_WIDTH, 1271);
	visionHandlerobj2.capture.set(CV_CAP_PROP_FRAME_HEIGHT, 410);

	//e->Result = trackBar1->Value;

	while (1)
	{
		visionHandlerobj1.capture.read(visionHandlerobj1.srcImg); // get the video frame

		cvtColor(visionHandlerobj1.srcImg, visionHandlerobj1.hsvImg, COLOR_BGR2HSV);

		visionHandlerobj2.capture.read(visionHandlerobj2.srcImg); // get the video frame

		cvtColor(visionHandlerobj2.srcImg, visionHandlerobj2.hsvImg, COLOR_BGR2HSV);

		if (visionHandlerobj1.calibrationMode == true)
		{
			if (visionHandlerobj1.trackbarCreated == false)
			{
				createTrackbars();
				visionHandlerobj1.trackbarCreated = true;
			}

			inRange(visionHandlerobj1.hsvImg, Scalar(H_MIN1, S_MIN1, V_MIN1), Scalar(H_MAX1, S_MAX1, V_MAX1), visionHandlerobj1.inrImg);

			visionHandlerobj1.morphOps(visionHandlerobj1.inrImg);

			visionHandlerobj1.trackFilteredObject(visionHandlerobj1, visionHandlerobj1.inrImg, visionHandlerobj1.hsvImg, visionHandlerobj1.srcImg, renderingStruct.topcamera_x, renderingStruct.topcamera_y, renderingStruct.topref_x, renderingStruct.topref_y);

			inRange(visionHandlerobj2.hsvImg, Scalar(H_MIN1, S_MIN1, V_MIN1), Scalar(H_MAX1, S_MAX1, V_MAX1), visionHandlerobj2.inrImg);

			visionHandlerobj2.morphOps(visionHandlerobj2.inrImg);

			visionHandlerobj2.trackFilteredObject(visionHandlerobj2, visionHandlerobj2.inrImg, visionHandlerobj2.hsvImg, visionHandlerobj2.srcImg, renderingStruct.sidecamera_x, renderingStruct.sidecamera_y, renderingStruct.sideref_x, renderingStruct.sideref_y);


		}

		else
		{
			TrackedObject needle("red");

			inRange(visionHandlerobj1.hsvImg, needle.getHSVmin(), needle.getHSVmax(), visionHandlerobj1.inrImg);

			visionHandlerobj1.morphOps(visionHandlerobj1.inrImg);

			visionHandlerobj1.trackFilteredObject(visionHandlerobj1, needle, visionHandlerobj1.inrImg, visionHandlerobj1.hsvImg, visionHandlerobj1.srcImg, renderingStruct.topcamera_x, renderingStruct.topcamera_y, renderingStruct.topref_x, renderingStruct.topref_y,1);

			inRange(visionHandlerobj2.hsvImg, needle.getHSVmin(), needle.getHSVmax(), visionHandlerobj2.inrImg);

			visionHandlerobj2.morphOps(visionHandlerobj2.inrImg);

			visionHandlerobj2.trackFilteredObject(visionHandlerobj2, needle, visionHandlerobj2.inrImg, visionHandlerobj2.hsvImg, visionHandlerobj2.srcImg, renderingStruct.sidecamera_x, renderingStruct.sidecamera_y, renderingStruct.sideref_x, renderingStruct.sideref_y,2);
			
			int i;
			
			if ((visionHandlerobj1.num_trackedobjects == visionHandlerobj2.num_trackedobjects)&&(savedRefPoints==true))
			{
				for ( i = 0; i < visionHandlerobj1.num_trackedobjects; i++)
				{					
						renderingStruct.topcamera_xstar[i] = (renderingStruct.topcamera_x[i] - renderingStruct.topref_x[1]) / (renderingStruct.topref_x[0] - renderingStruct.topref_x[1]);

						renderingStruct.topcamera_ystar[i] = (renderingStruct.topcamera_y[i] - renderingStruct.topref_y[1]) / (renderingStruct.topref_y[0] - renderingStruct.topref_y[1]);

						renderingStruct.sidecamera_xstar[i] = (renderingStruct.sidecamera_x[i] - renderingStruct.sideref_x[1]) / (renderingStruct.sideref_x[0] - renderingStruct.sideref_x[1]);

						renderingStruct.sidecamera_ystar[i] = (renderingStruct.sidecamera_y[i] - renderingStruct.sideref_y[1]) / (renderingStruct.sideref_y[0] - renderingStruct.sideref_y[1]);

					if (renderingStruct.topcamera_ystar[i] != 0)
					{
						renderingStruct.openGL_x1[i] = 1 - renderingStruct.topcamera_ystar[i];
						renderingStruct.openGL_x2[i] = renderingStruct.sidecamera_xstar[i];
					}
					else
						break;

				}
			}


			for (int iter = 0; iter < visionHandlerobj1.num_trackedobjects; iter++)
			{
			
				renderingStruct.openGL_x[iter] = 140*((renderingStruct.openGL_x1[iter] + renderingStruct.openGL_x2[iter]) / 2);
			    renderingStruct.openGL_y[iter] = 140*(1 - renderingStruct.topcamera_xstar[iter]);
				renderingStruct.openGL_z[iter] = 60*(1 - renderingStruct.sidecamera_ystar[iter]);

			}
			

			if (visionHandlerobj1.referenceMode == true)
			{
				TrackedObject refpoints("green");

				cvtColor(visionHandlerobj1.srcImg, visionHandlerobj1.hsvImg, COLOR_BGR2HSV);

				inRange(visionHandlerobj1.hsvImg, refpoints.getHSVmin(), refpoints.getHSVmax(), visionHandlerobj1.inrImg);

				visionHandlerobj1.morphOps(visionHandlerobj1.inrImg);

				visionHandlerobj1.trackFilteredObject(visionHandlerobj1, refpoints, visionHandlerobj1.inrImg, visionHandlerobj1.hsvImg, visionHandlerobj1.srcImg, renderingStruct.topcamera_x, renderingStruct.topcamera_y, renderingStruct.topref_x, renderingStruct.topref_y,1);

				cvtColor(visionHandlerobj2.srcImg, visionHandlerobj2.hsvImg, COLOR_BGR2HSV);

				inRange(visionHandlerobj2.hsvImg, refpoints.getHSVmin(), refpoints.getHSVmax(), visionHandlerobj2.inrImg);

				visionHandlerobj2.morphOps(visionHandlerobj2.inrImg);

				visionHandlerobj2.trackFilteredObject(visionHandlerobj2, refpoints, visionHandlerobj2.inrImg, visionHandlerobj2.hsvImg, visionHandlerobj2.srcImg, renderingStruct.sidecamera_x, renderingStruct.sidecamera_y, renderingStruct.sideref_x, renderingStruct.sideref_y,1);

				visionHandlerobj1.referenceMode = false;

				savedRefPoints = true;

			}


		}

		resize(visionHandlerobj1.srcImg, visionHandlerobj1.resizedsrcImg, cv::Size(700, 330));
		WebCamFeed1->Image = mat2bmp.Mat2Bimap(visionHandlerobj1.resizedsrcImg);

		resize(visionHandlerobj2.srcImg, visionHandlerobj2.resizedsrcImg, cv::Size(700, 330));
		WebCamFeed2->Image = mat2bmp.Mat2Bimap(visionHandlerobj2.resizedsrcImg);

		char key = (char)cv::waitKey(30);
		if (key == 27)
			break;
	}
}
private: System::Void checkBox1_CheckedChanged(System::Object^  sender, System::EventArgs^  e) {

	if (checkBox1->Checked == true)
	{
		visionHandlerobj1.calibrationMode = true;
	}

	else
	{
		visionHandlerobj1.calibrationMode = false;
	}
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {

	visionHandlerobj1.referenceMode = true;
}
private: System::Void HomeTab_Enter(System::Object^  sender, System::EventArgs^  e) {

	this->timerForOpenGLRender->Enabled = true;
	this->timerForOpenGLRender->Interval = 100;
	this->timerForOpenGLRender->Tick += gcnew System::EventHandler(this, &MainForm::timerForOpenGLRender_Tick);

	if (OpenGLcount == 0)
	{
		OpenGL = gcnew COpenGL(this->HomeTab, 800, 600);
		OpenGLcount = 1;
	}
}
private: System::Void timerForOpenGLRender_Tick(System::Object^  sender, System::EventArgs^  e) {

	UNREFERENCED_PARAMETER(sender);

	UNREFERENCED_PARAMETER(e);

	OpenGL->Render(renderingStruct); // this function is called every 100 ms and the information from the mainform.h is communicated to the Graphic window (OpenGlRender.h) through this renderingstruct (structure).

	OpenGL->SwapOpenGLBuffers();
}
private: System::Void VisionTab_Enter(System::Object^  sender, System::EventArgs^  e) {

	this->timerForOpenGLRender->Enabled = false;
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

	if (cameraViewCount == 0)
	{
		renderingStruct.cameraView = "frontview";
		cameraViewCount++;
		label1->Text = "Front View";
	}
	else if (cameraViewCount == 1)
	{
		renderingStruct.cameraView = "sideview";
		cameraViewCount++;
		label1->Text = "Side View";
	}
	else if (cameraViewCount == 2)
	{
		renderingStruct.cameraView = "isometricview";
		cameraViewCount++;
		label1->Text = "Isometric View";
	}
	else if (cameraViewCount == 3)
	{
		renderingStruct.cameraView = "topview";
		cameraViewCount = 0;
		label1->Text = "Top View";
	}

}
private: System::Void EngageMotor_HomeTab_Click(System::Object^  sender, System::EventArgs^  e) {

	this->DisEngagingbutton_HomeTab->Enabled = true;
	backgroundWorkerForNovintInteraction->RunWorkerAsync();
}
private: System::Void DisEngagingbutton_HomeTab_Click(System::Object^  sender, System::EventArgs^  e) {

	this->backgroundWorkerForNovintInteraction->CancelAsync();
	this->DisEngagingbutton_HomeTab->Enabled = false;
}
private: System::Void setobstacle_1_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.removeobstacle_1 = false;

	if (obstacle1_x->Text == "")
	{
		renderingStruct.obstacles_array[0] = 9875;  //renderingStruct is a structure defined in OpenGlRender.h and accessed here to store obstacle positions and sizes

		renderingStruct.obstacles_array[1] = 9875;

		renderingStruct.obstacles_array[2] = 9875;

		renderingStruct.obstacles_array[3] = 9875;
	}
	else
	{
		renderingStruct.obstacles_array[0] = (double)(Convert::ToDouble(obstacle1_x->Text));

		renderingStruct.obstacles_array[1] = (double)(Convert::ToDouble(obstacle1_y->Text));

		renderingStruct.obstacles_array[2] = (double)(Convert::ToDouble(obstacle1_z->Text));

		renderingStruct.obstacles_array[3] = (double)(Convert::ToDouble(obstacle1_r->Text));

	}


}
private: System::Void setobstacle_2_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.removeobstacle_2 = false;

	if (obstacle2_x->Text == "")
	{
		renderingStruct.obstacles_array[4] = 9875;

		renderingStruct.obstacles_array[5] = 9875;

		renderingStruct.obstacles_array[6] = 9875;

		renderingStruct.obstacles_array[7] = 9875;
	}
	else
	{
		renderingStruct.obstacles_array[4] = (double)(Convert::ToDouble(obstacle2_x->Text));

		renderingStruct.obstacles_array[5] = (double)(Convert::ToDouble(obstacle2_y->Text));

		renderingStruct.obstacles_array[6] = (double)(Convert::ToDouble(obstacle2_z->Text));

		renderingStruct.obstacles_array[7] = (double)(Convert::ToDouble(obstacle2_r->Text));
	}

}
private: System::Void setobstacle_3_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.removeobstacle_3 = false;

	if (obstacle3_x->Text == "")
	{
		renderingStruct.obstacles_array[8] = 9875;

		renderingStruct.obstacles_array[9] = 9875;

		renderingStruct.obstacles_array[10] = 9875;

		renderingStruct.obstacles_array[11] = 9875;
	}

	else
	{
		renderingStruct.obstacles_array[8] = (double)(Convert::ToDouble(obstacle3_x->Text));

		renderingStruct.obstacles_array[9] = (double)(Convert::ToDouble(obstacle3_y->Text));

		renderingStruct.obstacles_array[10] = (double)(Convert::ToDouble(obstacle3_z->Text));

		renderingStruct.obstacles_array[11] = (double)(Convert::ToDouble(obstacle3_r->Text));
	}


}
private: System::Void setstart_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.entryradius = 5;

	if (start_x->Text == "")
	{
		renderingStruct.start_position[0] = 9875;

		renderingStruct.start_position[1] = 9875;

		renderingStruct.start_position[2] = 9875;

	}

	else
	{
		renderingStruct.start_position[0] = (double)(Convert::ToDouble(start_x->Text));

		renderingStruct.start_position[1] = (double)(Convert::ToDouble(start_y->Text));

		renderingStruct.start_position[2] = (double)(Convert::ToDouble(start_z->Text));
	}


}
private: System::Void settarget_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.targetradius = 5;

	if (target_x->Text == "")
	{
		renderingStruct.target_position[0] = 9875;

		renderingStruct.target_position[1] = 9875;

		renderingStruct.target_position[2] = 9875;
	}

	else
	{
		renderingStruct.target_position[0] = (double)(Convert::ToDouble(target_x->Text));

		renderingStruct.target_position[1] = (double)(Convert::ToDouble(target_y->Text));

		renderingStruct.target_position[2] = (double)(Convert::ToDouble(target_z->Text));
	}

}

private: System::Void generatepath_Click(System::Object^  sender, System::EventArgs^  e) {

	this->backgroundWorkerForPathGeneration->RunWorkerAsync();


}

private: System::Void backgroundWorkerForPathGeneration_DoWork(System::Object^  sender, System::ComponentModel::DoWorkEventArgs^  e) {

	BackgroundWorker^ pathgenerator = dynamic_cast<BackgroundWorker^>(sender);

	if (pathgenerator->CancellationPending)
	{
		Thread::Sleep(1200);

		e->Cancel = true;
	}

	delete[] renderingStruct.x_path;					//destroy everything in here
	delete[] renderingStruct.y_path;
	delete[] renderingStruct.z_path;

	renderingStruct.x_path = new double[1];  

	renderingStruct.x_path[0] = renderingStruct.start_position[0];

	renderingStruct.y_path = new double[1];

	renderingStruct.y_path[0] = renderingStruct.start_position[1];

	renderingStruct.z_path = new double[1];

	renderingStruct.z_path[0] = renderingStruct.start_position[2];

	int n = 1;

	while (sqrt(pow(renderingStruct.x_path[n-1] - renderingStruct.target_position[0], 2) + pow(renderingStruct.y_path[n-1] - renderingStruct.target_position[1], 2) + pow(renderingStruct.z_path[n - 1] - renderingStruct.target_position[2], 2)) > 0.5)
	{
		n++;

		int i;
		double delta = 0.0000000001;
		double * newx_path = new double[n];
		double * newy_path = new double[n];
		double * newz_path = new double[n];

		for (i = 0; i < n-1; i++)	//old array size
		{
			newx_path[i] = renderingStruct.x_path[i];
			newy_path[i] = renderingStruct.y_path[i];
			newz_path[i] = renderingStruct.z_path[i];
		}

		derivative_x = (pathGeneratorobj.function(renderingStruct.x_path[i-1]+delta, renderingStruct.y_path[i-1], renderingStruct.z_path[i-1], renderingStruct.target_position, renderingStruct.obstacles_array, i) - pathGeneratorobj.function(renderingStruct.x_path[i-1], renderingStruct.y_path[i - 1], renderingStruct.z_path[i - 1], renderingStruct.target_position, renderingStruct.obstacles_array, i)) / delta;

		derivative_y = (pathGeneratorobj.function(renderingStruct.x_path[i - 1], renderingStruct.y_path[i - 1]+delta, renderingStruct.z_path[i - 1], renderingStruct.target_position, renderingStruct.obstacles_array, i) - pathGeneratorobj.function(renderingStruct.x_path[i - 1], renderingStruct.y_path[i - 1], renderingStruct.z_path[i - 1], renderingStruct.target_position, renderingStruct.obstacles_array, i)) / delta;

		derivative_z = (pathGeneratorobj.function(renderingStruct.x_path[i - 1], renderingStruct.y_path[i - 1], renderingStruct.z_path[i - 1]+delta, renderingStruct.target_position, renderingStruct.obstacles_array, i) - pathGeneratorobj.function(renderingStruct.x_path[i - 1], renderingStruct.y_path[i - 1], renderingStruct.z_path[i - 1], renderingStruct.target_position, renderingStruct.obstacles_array, i)) / delta;

		newx_path[n - 1] = newx_path[n - 2] - 0.005*derivative_x;
		newy_path[n - 1] = newy_path[n - 2] - 0.005*derivative_y;
		newz_path[n - 1] = newz_path[n - 2] - 0.005*derivative_z;

		delete[] renderingStruct.x_path;					//destroy everything in here
		delete[] renderingStruct.y_path;
		delete[] renderingStruct.z_path;


		renderingStruct.x_path = new double[n];			//reset with new size // dynamic increment of array sizes
		renderingStruct.y_path = new double[n];
		renderingStruct.z_path = new double[n];

		for (i = 0; i < n; i++)
		{
			renderingStruct.x_path[i] = newx_path[i];
			renderingStruct.y_path[i] = newy_path[i];
			renderingStruct.z_path[i] = newz_path[i];

		}
		delete[] newx_path;
		delete[] newy_path;
		delete[] newz_path;

	}


	renderingStruct.pathpoints_num = n;

	//renderingStruct.x_path_smooth = new double[n];
	//renderingStruct.y_path_smooth = new double[n];
	//renderingStruct.z_path_smooth = new double[n];

	//int span = 20;
	//int count, temp2;
	//double temp, tempx, tempy, tempz;

	//for (count = 0; count < n; count++)
	//{
	//	if (count < span)
	//	{
	//		 temp = count;
	//	}
	//	else
	//	{
	//		 temp = span;
	//	}

	//	int tempx = 0, tempy=0, tempz=0;

	//	for (temp2 = temp; temp2 >= 0; temp2--)
	//	{
	//		tempx = renderingStruct.x_path[count - temp2] + tempx;
	//		tempy = renderingStruct.y_path[count - temp2] + tempy;
	//		tempz = renderingStruct.z_path[count - temp2] + tempz;


	//	}


	//	renderingStruct.x_path_smooth[count] = tempx / (temp + 1);
	//	renderingStruct.y_path_smooth[count] = tempy / (temp + 1);
	//	renderingStruct.z_path_smooth[count] = tempz / (temp + 1);
	//}

	renderingStruct.pathdone = true; 

}
private: System::Void backgroundWorkerForPathGeneration_RunWorkerCompleted(System::Object^  sender, System::ComponentModel::RunWorkerCompletedEventArgs^  e) {

	if (e->Error != nullptr)
	{
		MessageBox::Show(e->Error->Message);
	}
}
private: System::Void removeobstacle_1_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.obstacles_array[3] = 0;
}
private: System::Void removeobstacle_2_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.obstacles_array[7] = 0;

}
private: System::Void removeobstacle_3_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.obstacles_array[11] = 0;
}
private: System::Void removestart_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.entryradius = 0;
}
private: System::Void removetarget_Click(System::Object^  sender, System::EventArgs^  e) {

	renderingStruct.targetradius = 0;
}
};
}
