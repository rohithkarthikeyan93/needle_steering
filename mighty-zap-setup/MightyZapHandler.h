#include <MFCMightyZapExample.h>
#include <MFCMightyZapExampleDlg.h>
#include <MightyZap.h>


#define PORT_OFF 0

namespace MightyZapHandler
{

	class mightyZapHandler
	{

	public:

		MightyZap * mightyzap;

		CString p_name = _T("COM5");
		CString id1_str = _T("001");
		CString id2_str = _T("002");
		CString id3_str = _T("003");
		CString id4_str = _T("004");

		const int motor1_neutral = 1897;// 1752;
		const int motor3_neutral = 1913;// 2440;
		const int motor2_neutral = 1891; // 2091;
		const int motor4_neutral = 1804; // 1830;

		int addr_force = 0x8A;
		int addr_presentposition = 0x8C;
		int addr_goalposition = 0x86;
		int addr_moving = 0x96;

		int limitingForce = 600;

		int position_1, position_1_after40ms, position_1_after80ms, force_1, position_3, position_3_after40ms, position_3_after80ms, force_3;

		bool portOpenState = PORT_OFF;

		void initializeMightyZapMotors(mightyZapHandler &mightyZapHandlerobj);

		void mapNovintToNeedleTip(double novint_calibrated_z, double novint_calibrated_y, mightyZapHandler &mightyZapHandlerobj);


	};
}
